/*!\file
    All the general information to be printed at program execution is
    collected here.
*/

#include <stdio.h>
#include <string.h>

#include "msg.h"
#include "def.h"

/*!
    The function in this file prints out general information about the program
    execution, for the chosen set of options. This includes explanatory text
    strings about the chosen program cycle and how the program can be stopped.
*/
void firstloop_verbosemsg(int only_sact, char alive_char, char cycle_char)
{
    const char quitinfo_msg[LINELEN] = QUITINFOMSG;

    const char h_msg[] = "h: "HEARTBEATPULSE".\n";
    const char H_msg[] = "H: "HEARTBEATPULSEBLINK".\n";
    const char q_msg[] = "q: "NOBLINKPULSE".\n";
    const char s_msg[] = "s: "SINGLEBLINKPULSE".\n";
    const char d_msg[] = "d: "DOUBLEBLINKBURSTPULSE".\n";
    const char t_msg[] = "t: "TRIPLEBLINKBURSTPULSE".\n";
    const char L_msg[] = "L: "SYSTEMCPULOADSTATUS".\n";
    const char n_msg[] = "n: "NETWORKINTERFACESTATUS".\n";
    const char m_msg[] = "m: "HEARTBEATPULSENETSTATUS".\n";
    const char N_msg[] = "N: "NETWORKTRAFFIC".\n";

    /* This variable is also a message buffer so we make it large. */
    char startup_msg[5*LINELEN] = STARTUPMSG; /* Is appended to below. */

    char prog_descr[LINELEN] = {'\0'}; /* One line has to be enough, really. */

    if (only_sact == 1) {
        switch (alive_char) {
            case 'h':
                strcpy(prog_descr, h_msg);
                break;
            case 'H':
                strcpy(prog_descr, H_msg);
                break;
            case 's':
                strcpy(prog_descr, s_msg);
                break;
            case 'd':
                strcpy(prog_descr, d_msg);
                break;
            case 't':
                strcpy(prog_descr, t_msg);
                break;
            case 'q':
                strcpy(prog_descr, q_msg);
                break;
            default:
                strcpy(prog_descr, h_msg);
        }
    } else {
        switch (cycle_char) {
            case 'L':
                strcpy(prog_descr, L_msg);
                break;
            case 'n':
                strcpy(prog_descr, n_msg);
                break;
            case 'm':
                strcpy(prog_descr, m_msg);
                break;
            case 'N':
                strcpy(prog_descr, N_msg);
                break;
            default:
                strcpy(prog_descr, n_msg);
        }
    }

    const char led_blinkmsg1[] = BLINKMSG1, led_blinkmsg2[] = BLINKMSG2;

    switch (cycle_char) {
        case 'h':
        case 'H':
        case 'q':
        case 's':
        case 'd':
        case 't':
            strcat(startup_msg, prog_descr);
            strcat(startup_msg, quitinfo_msg);
            fprintf(stderr, "%s", startup_msg);
            break;
        case 'L':
            strcat(startup_msg, prog_descr);
            fprintf(stderr, "%s", startup_msg);
            fprintf(stderr, "%s", quitinfo_msg);
            break;
        case 'n':
        case 'm':
            strcat(startup_msg, prog_descr);
            strcat(startup_msg, led_blinkmsg1);
            fprintf(stderr, "%s", startup_msg);
            fprintf(stderr, "%s", quitinfo_msg);
            break;
        case 'N':
            strcat(startup_msg, prog_descr);
            strcat(startup_msg, led_blinkmsg2);
            fprintf(stderr, "%s", startup_msg);
            fprintf(stderr, "%s", quitinfo_msg);
            break;
        default:
            strcat(startup_msg, prog_descr);
            strcat(startup_msg, quitinfo_msg);
            fprintf(stderr, "%s", startup_msg);
    }
}
