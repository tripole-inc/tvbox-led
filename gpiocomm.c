/*!\file
    The functions here are used to set up communication channel to the GPIO via
    a character device and then a GPIO line handle, and to perform queries to
    the GPIO chip and line. The LED is at the receiving end of the line handle.
*/

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/gpio.h>
#include <linux/stddef.h>

extern int debug_level;
extern int verbosity_level;

#include "def.h"
#include "gpiocomm.h"

/*!
    The GPIO device is set up (initiated) using a GPIO character device. As
    part of this procedure, a GPIO line handle is obtained which is then used
    to send further data to the GPIO device (and turn the LED on/off). This
    function also closes the handle when it is not needed anymore.
 */
int gpio_updown(struct gpiohandle_data *gpioh_data,
                struct gpiohandle_request *gpioh_req,
                int gpioh_open)
{
    /*!
        A good reference to get some understanding of the mechanics underlying
        GPIO setup and communication is the Linux kernel source file
        tools/gpio/gpio-utils.c
    */

    int ret, fd_gpiochardev;

    if (gpioh_open == 1) {
        /*  Open the character device for the GPIO chip, for comms setup/def. */
        char chrdev_name[] = GPIOCHIPDEV;
        fd_gpiochardev = open(chrdev_name, 0);

        if (fd_gpiochardev == -1) {
            if (debug_level > 0)
               fprintf(stderr,"%s: Error: Failed to open %s (%s)",
                       __func__, chrdev_name, strerror(errno));

            exit(1);
        }

        /* Request a GPIO line handle. */
        gpioh_req->lineoffsets[0] = GPIOLINENUM;
        gpioh_req->lines = 1;
        gpioh_req->flags = GPIOHANDLE_REQUEST_OUTPUT; /* Using default flags. */
        /* According to gpio-utils.c we should copy memory contents here. */
        memcpy(gpioh_req->default_values, gpioh_data,
               sizeof(gpioh_req->default_values));

        char gpio_linetag[40] = GPIOLINETAG;
        char gpio_linenumstr[4] = {'\0', '\0', '\0', '\0'};

        sprintf(gpio_linenumstr, "%d", GPIOLINENUM);
        strcat(gpio_linetag, gpio_linenumstr);
        strcpy(gpioh_req->consumer_label, gpio_linetag);
        if (verbosity_level > 1)
            fprintf(stderr, "GPIO consumer label: \"%s\"\n",
                            gpioh_req->consumer_label);

        /* The line handle is used to communicate with the GPIO LED pin(s). */
        ret = ioctl(fd_gpiochardev, GPIO_GET_LINEHANDLE_IOCTL, gpioh_req);
        if (ret == -1) {
            if (debug_level > 0)
                fprintf(stderr, "%s: Error: "
                                "Failed to issue GET LINEHANDLE IOCTL "\
                                "%d (%s)\n",
                                __func__, ret, strerror(errno));

            exit(1);
        }

        /* Optional: Get info about the GPIO device (chip and i/o line/pin). */
        /* This doesn't provide any useful info currently so it is disabled. */
        /* if (verbosity_level > 1)
            gpio_getchipinfo(fd_gpiochardev); */

        /* The line handle to the GPIO device is still open after this. */
        if (close(fd_gpiochardev) == -1) {
            if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to close GPIO char. device "
                                "file %s (%s).",
                                __func__, chrdev_name, strerror(errno));

            exit(1);
        }

    } else {
        ret = close(gpioh_req->fd);
        if (ret == -1) {
            if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to close "\
                                "GPIO LINEHANDLE device file (%s).",
                                  __func__, strerror(errno));
       }

    }

    return ret;
}

/*!
   This function queries the character device set up for the GPIO and retrieves
   info about the GPIO chip and lines. (Helper function, used for debugging.)
*/
int gpio_getchipinfo(int fd_chrdev)
{
    struct gpiochip_info cinfo;

    int ret = ioctl(fd_chrdev, GPIO_GET_CHIPINFO_IOCTL, &cinfo);
    if (ret == -1) {
        fprintf(stderr, "%s: Warning: "\
                        "Failed to do GPIO GET CHIPINFO IOCTL %d (%s)\n",
                        __func__, ret, strerror(errno));
    } else {
        fprintf(stdout, "GPIO chip info: "\
                        "name %s, label \"%s\", %u GPIO lines\n",
                        cinfo.name, cinfo.label, cinfo.lines);
    }

    struct gpioline_info linfo;

    ret = ioctl(fd_chrdev, GPIO_GET_LINEINFO_IOCTL, &linfo);
    if (ret == -1) {
        fprintf(stderr, "%s: Warning: "\
                        "Failed to do GPIO GET LINEINFO IOCTL %d (%s)\n",
                         __func__, ret, strerror(errno));
    } else {
        /* On my Ugoos AM6 plus linfo.name is empty and linfo.lineoffset is
           zero. The latter indicats that the offset refers to the line ctl
           and not not the gpio pin itself (a.k.a. "above my paygrade"). */
        fprintf(stdout, "GPIO line info: offset %2d, flags %2d\n"\
                        "GPIO line info: name %s, consumer %s\n",
                        linfo.line_offset, linfo.flags,
                        linfo.name, linfo.consumer);
    }

    return ret;
}
