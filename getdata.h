/*!\file
    Function declaration for the data collection functionality.
*/

#ifndef GETDATA_H
#define GETDATA_H

struct netstat_data;
struct diskstat_data;
struct diskstat_table;

/*! Gets the average CPU load over all cores. */
float cpu_getload(void);

/*! Determines the state for a network interface (up/down). */
int iface_getstate(const char *iface_name);

/*! Collects network traffic statistics. */
int iface_getstats(struct netstat_data *if_data);

/*! Collects disk utilization/throughput statistics. */
int disk_getstats(struct diskstat_table *diskst_table);

/*! Reads the config file (if it exists). */
int config_read(const char *conf_file, const char *prg_name,
                char* argv[], char *v_arg, char **vargp);

#endif
