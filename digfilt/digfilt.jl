# This file contains julia code to design a digital filter of the biquad type,
# as well as a simple one-pole filter, for comparison. The results can be
# plotted and also saved to file. The code was prepared for the design of a
# filter for network interface load values to be used in the program tvbox-led.
# Author: Tripole, 2021-01-23.

# The homepage of The Julia Programming Language is https://julialang.org

"""
    Module with code to design and study a simple digital filter of the biquad
    type, and to compare it with a one-pole filter.
    Requires: DSP, Plots

Usage:

(1) Open the file digfilt.jl and fiddle with the parameters, then save.

(2) On the julia command line do:

julia> include("digfilt.jl")

(3) Plot the result, for example (for more examples, look into the code):
    (Do not close the plot window between plots; you'll get a "broken pipe")

julia> display(digfilt.bifph) # Frequency response

julia> display(digfilt.bipph) # Phase response

julia> display(digfilt.biiph) # Impulse response

julia> display(digfilt.bisph) # Step response

"""
module digfilt

# The parameters values below for the two filters, the biquad filter and the
# exponential averaging filter (one-pole filter), are chosen (somewhat ad hoc)
# so that they both have comparable low frequency (averaging) properties (in
# terms of magnitude of the frequency response), a similar impulse response
# (sort of) and step response. However, they have different high frequency
# response, where the one-pole filter has a larger high frequency gain.
# Despite this, the biquad filter has better tracking properties for the type
# of input sequences we focus on (variations in network interface load values).
# This can be explained by the difference in phase response (see the plots).
# The biquad filter offers the possibility of adding some phase advancement
# ("derivative action") at certain frequencies and this makes it possible to
# obtain a bit better tracking properties for our intended application.
# (Experimentally it was found that the best performance could be archived by
# maximizing phase advancement around normalized frequencies of about 0.5, i.e.
# for oscillations with a period of about four samples.)

# A note on the application of the code in this file to the program tvbox-led:

# In tvbox-led the so called second direct form for the biquad filter is used.
# This filter structure is obtained by factoring a standard second order
# transfer function H(z) = B(z)/A(z) as a cascade H(z) = B(z) * (1/A(z)) and
# writing down the corresponding time domain difference equations, which are
#   y_n = b_0 v_n +b_1 v_{n-1} +b_2 v_{n-2} ,
#   x_n = 1 + a_1 v_{n-1} +a_2 v_{n-2} ,
# where v is the new intermediate variable (appearing as the output of the
# first filter with transfer function 1/A(z)). The main advantage with this
# representation is that it requires only one delay line (for the v's).
# (The proper initial values for the v's can be calculated from the first two
# x's and y's by using the equations above and their one-step shifted versions,
# but we shall not consider this issue here; we assume zero initial values.)

# These julia packages must be installed:
using DSP, Plots
# https://docs.juliadsp.org/stable/filters
# https://www.math.purdue.edu/~allen450/Plotting-Tutorial.html

# -- Biquad filter object definition and instantiation. --
# The transfer function is H(z) = (b0 z^2 +b1 z +b2)/(1 +a1 z +a2 z^2) and the
# corresponding difference equation is
# y_{n-2} +a1 y_{n-1} +a2 y_{n-2} = b_0 x_n + b1 x_{n-1} + b2 x_{n-2} .
# For (strict, asymptotic) stability the two zeros (roots) of the denominator
# polynomial p(z) = 1 +a1 z +a2 z^2 must lie strictly inside the unit circle in
# the complex plane. We make it easy to fulfill this below by factoring p as
# p(z) = (z - z_0) (z -\bar{z}_0) and use polar coordinates for the two complex
# conjugate roots z_0, \bar{z}_0. So, if we write z_0 = R e^{i \theta} then we
# have p(z) = z^2 -2 R \cos(\theta) z + R^2. (We proceed analogously for the
# numerator polynomial in H.)

# As our "design method" we use pole-zero placement (+ eyeballing the result).
nullr = 0.2
nullVarphi = 0.65*pi
poleR = 0.5
poleTheta = 0.2*pi

statGain =
  (1-2*nullr*cos(nullVarphi)+nullr^2)/(1-2*poleR*cos(poleTheta)+poleR^2)
b0 = 1/statGain # Gives unity gain of the filter below.

b1 = -2*nullr*cos(nullVarphi)*b0
b2 = nullr^2*b0
a1 = -2*poleR*cos(poleTheta)
a2 = poleR^2

# It is always good to know a little about rounding sensitivity for a biquad.
ndigits = 5
a1r = round(a1, digits=ndigits)
a2r = round(a2, digits=ndigits)
b0r = round(b0, digits=ndigits)
b1r = round(b1, digits=ndigits)
b2r = round(b2, digits=ndigits)

biFilt = DSP.Filters.Biquad(b0r, b1r, b2r, a1r, a2r)

# -- Exponential averaging (one-pole) filter definition and instantiation. --
# The transfer function is H(z) = z/(z -a) = 1/(1-a z^{-1}) and the difference
# equation is y_n = a y_ {n-1} +x_n, which (for |a| < 1) has the solution
# y_n = \sum_{k=0}^{\infty} a^{k} x_{n-k}. (One initial condition, e.g.
# y_{n-1}, needs to be specified for uniqueness; we set y_{n-1} = 0 here.)
avgfact = 0.4 # Our name for the constant 'a', the "averaging factor".
# statGain2 = 1.0/(1.0 -avgfact)
exFilt  = DSP.Filters.PolynomialRatio([0.0; 1.0-avgfact], [1.0; -avgfact])

# -- Frequency domain response (both filters). --
nticks = 250
w = range(0, stop=pi, length=nticks)  # Radians/sec
q = range(0, stop=1.0, length=nticks) # Normalized [0,1]

biFreqz  = DSP.Filters.freqz(biFilt, w)  # ::Array{Complex{Float64},1}
biPhasez = DSP.Filters.phasez(biFilt, w) # ::Array{Float64,1}

exFreqz  = DSP.Filters.freqz(exFilt, w)  # ::Array{Complex{Float64},1}
exPhasez = DSP.Filters.phasez(exFilt, w) # ::Array{Float64,1}

# -- Time domain standard response (both filters). --
nssteps = 5
nlsteps = nssteps+3
timearr = range(0, stop=nlsteps-1, length=nlsteps)

biImpz  = DSP.Filters.impz(biFilt, nlsteps)
biStepz = DSP.Filters.stepz(biFilt, nlsteps)

exImpz  = DSP.Filters.impz(exFilt, nlsteps)
exStepz = DSP.Filters.stepz(exFilt, nlsteps)

# -- Time domain response for some special input sequences (both filters). --
# These sequences are intended to be examples of input values of network load
# values in our application, but they are chosen more or less ad hoc so it
# should not be difficult to improve the tuning with other test sequences.
inpseq1 = [0.5; 1.0; 0.5; 0.0; 0.5; zeros(nlsteps-nssteps)]
inpseq2 = [1.0; 0.5; 0.5; 0.5; 0.0; zeros(nlsteps-nssteps)]
inpseq3 = [1.0; 0.5; 0.5; 1.0; 0.0; zeros(nlsteps-nssteps)]

outpseq1 = inpseq1[1]*biImpz[1:nlsteps]
outpseq2 = inpseq2[1]*biImpz[1:nlsteps]
outpseq3 = inpseq3[1]*biImpz[1:nlsteps]

exoutseq1 = inpseq1[1]*exImpz[1:nlsteps]
exoutseq2 = inpseq2[1]*exImpz[1:nlsteps]
exoutseq3 = inpseq3[1]*exImpz[1:nlsteps]

# Due to julia's new (post v1.0) scoping rules (and the fact that this code
# will be executed at the top level in the REPL) we use a globals hack here.
for i=2:nssteps
  shiftseq = [zeros(i-1); biImpz[i:nlsteps]]
  slideseq = [zeros(i-1); exImpz[i:nlsteps]]
  global outpseq1 += inpseq1[i]*shiftseq
  global outpseq2 += inpseq2[i]*shiftseq
  global outpseq3 += inpseq3[i]*shiftseq
  global exoutseq1 += inpseq1[i]*slideseq
  global exoutseq2 += inpseq2[i]*slideseq
  global exoutseq3 += inpseq3[i]*slideseq
end

# -- Plotting --

# - Biquad only. --
bifph = Plots.plot(q, abs.(biFreqz), legend=:topleft,
        title="Frequency response (magnitude)", xaxis=("normalized freq"),
        yaxis=("magnitude [-]"), label=("biquad"), ylims = (0.0, 1.5),
        st = :line)

bipph = Plots.plot(q, biPhasez, legend=:bottomleft,
        title="Frequency response (phase)", xaxis=("normalized freq"),
        yaxis=("phase [radians]"), label=("biquad"), st = :line)

biiph = Plots.plot(timearr[1:nssteps], biImpz[1:nssteps], legend=:topleft,
        title="Impulse response", xaxis=("time"),
        yaxis=("value [-]"), label=("biquad"), ylims = (-0.8,1.2),
        st = :line, markershape = :circle)

bisph = Plots.plot(timearr, biStepz, legend=:bottomright,
        title="Step response", xaxis=("time"),
        yaxis=("value [-]"), label=("biquad"), ylims = (0,1.2),
        st = :line, markershape = :circle)

bisrph = Plots.plot(timearr, sqrt.(biStepz), legend=:bottomright,
        title="Square root of step response", xaxis=("time"),
        yaxis=("value [-]"), label=("biquad"), ylims = (0,1.2),
        st = :line, markershape = :circle)

bir1ph = Plots.plot(timearr, [inpseq1 outpseq1], legend=:topright,
         title="Forced response, sequence 1, biqad filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

bir2ph = Plots.plot(timearr, [inpseq2 outpseq2], legend=:topright,
         title="Forced response, sequence 2, biquad filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

bir3ph = Plots.plot(timearr, [inpseq3 outpseq3], legend=:topright,
         title="Forced response, sequence 3, biquad filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

# - One-pole only. --
exfph = Plots.plot(q, abs.(exFreqz), legend=:topleft,
        title="Frequency response (magnitude)", xaxis=("normalized freq"),
        yaxis=("magnitude [-]"), label=("onepole"), ylims = (0.0, 1.5),
        st = :line)

expph = Plots.plot(q, exPhasez, legend=:bottomleft,
        title="Frequency response (phase)", xaxis=("normalized freq"),
        yaxis=("phase [radians]"), label=("onepole"), st = :line)

exiph = Plots.plot(timearr[1:nssteps], exImpz[1:nssteps], legend=:topleft,
        title="Impulse response", xaxis=("time"),
        yaxis=("value [-]"), label=("onepole"), ylims = (-0.8,1.2),
        st = :line, markershape = :circle)

exsph = Plots.plot(timearr, exStepz, legend=:bottomright,
        title="Step response", xaxis=("time"),
        yaxis=("value [-]"), label=("onepole"), ylims = (0,1.2),
        st = :line, markershape = :circle)

exsrph = Plots.plot(timearr, sqrt.(exStepz), legend=:bottomright,
        title="Square root of step response", xaxis=("time"),
        yaxis=("value [-]"), label=("onepole"), ylims = (0,1.2),
        st = :line, markershape = :circle)

exr1ph = Plots.plot(timearr, [inpseq1 exoutseq1], legend=:topright,
         title="Forced response, sequence 1, onepole filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

exr2ph = Plots.plot(timearr, [inpseq2 exoutseq2], legend=:topright,
         title="Forced response, sequence 2, onepole filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

exr3ph = Plots.plot(timearr, [inpseq3 exoutseq3], legend=:topright,
         title="Forced response, sequence 3, onepole filter", xaxis=("time"),
         yaxis=("value [-]"), label=["input" "output"], ylims = (-0.5,1.2),
         st = :line, markershape = :circle)

# - Both filters. -
befph = Plots.plot(q, [abs.(biFreqz) abs.(exFreqz)], legend=:topleft,
        title="Frequency response (magnitude)", xaxis=("normalized freq"),
        yaxis=("magnitude [-]"), label=(["biquad" "onepole"]),
        ylims = (0.0, 1.5), st = :line)

bepph = Plots.plot(q, [biPhasez exPhasez], legend=:bottomleft,
        title="Frequency response (phase)", xaxis=("normalized freq"),
        yaxis=("phase [radians]"), label=(["biquad" "onepole"]), st = :line)

beiph = Plots.plot(timearr[1:nssteps], [biImpz[1:nssteps] exImpz[1:nssteps]],
        legend=:topleft, title="Impulse response", xaxis=("time"),
        yaxis=("value [-]"), label=(["biquad" "onepole"]), ylims = (-0.8,1.2),
        st = :line, markershape = :circle)

besph = Plots.plot(timearr, [biStepz exStepz], legend=:bottomright,
        title="Step response", xaxis=("time"),
        yaxis=("value [-]"), label=(["biquad" "onepole"]), ylims = (0,1.2),
        st = :line, markershape = :circle)

besrph = Plots.plot(timearr, [sqrt.(biStepz) sqrt.(exStepz)],
        legend=:bottomright,
        title="Square root of step response", xaxis=("time"),
        yaxis=("value [-]"), label=(["biquad" "onepole"]), ylims = (0,1.2),
        st = :line, markershape = :circle)

ber1ph = Plots.plot(timearr, [inpseq1 outpseq1 exoutseq1], legend=:topright,
         title="Forced response, sequence 1",
         xaxis=("time"), yaxis=("value [-]"),
         label=["input" "output-biquad" "output-onepole"],
         ylims = (-0.5,1.2), st = :line, markershape = :circle)

ber2ph = Plots.plot(timearr, [inpseq2 outpseq2 exoutseq2], legend=:topright,
         title="Forced response, sequence 2",
         xaxis=("time"), yaxis=("value [-]"),
         label=["input" "output-biquad" "output-onepole"],
         ylims = (-0.5,1.2), st = :line, markershape = :circle)

ber3ph = Plots.plot(timearr, [inpseq3 outpseq3 exoutseq3], legend=:topright,
         title="Forced response, sequence 3",
         xaxis=("time"), yaxis=("value [-]"),
         label=["input" "output-biquad" "output-onepole"],
         ylims = (-0.5,1.2),st = :line, markershape = :circle)

function savePlots(typestr::String)
  # The package Plots has support for these formats (functions):
  # png, svg, pdf, ps, eps, tex, tikz (alias for tex), json, html, txt
  if (typestr == "bi")
    Plots.png(bifph, "bif") # Change to Plots.svg() for svg plt, etc.
    Plots.png(bipph, "bip")
    Plots.png(biiph, "bii")
    Plots.png(bisph, "bis")
    Plots.png(bisrph, "bisr")
    Plots.png(bir1ph, "bir1")
    Plots.png(bir2ph, "bir2")
    Plots.png(bir3ph, "bir3")
  elseif (typestr == "ex")
    Plots.png(exfph, "exf")
    Plots.png(expph, "exp")
    Plots.png(exiph, "exi")
    Plots.png(exsph, "exs")
    Plots.png(exsrph, "exsr")
    Plots.png(exr1ph, "exr1")
    Plots.png(exr2ph, "exr2")
    Plots.png(exr3ph, "exr3")
  else
    Plots.png(befph, "bef")
    Plots.png(bepph, "bep")
    Plots.png(beiph, "bei")
    Plots.png(besph, "bes")
    Plots.png(besrph, "besr")
    Plots.png(ber1ph, "ber1")
    Plots.png(ber2ph, "ber2")
    Plots.png(ber3ph, "ber3")
  end
end

# Plotting is now done with (assuming this code module has been loaded)
# julia> display(digfilt.bifph)
# julia> display(digfilt.expph)
# julia> display(digfilt.beiph)
# etc.

# Saving to file: # {"bi", "ex", "be"} biquad, exponential, biquad+exponential
# julia> digfilt.savePlots("bi")

# To view (unexported) variables (here stacked in an array) do like
# julia> [digfilt.a1r; digfilt.a2r; digfilt.b0r; digfilt.b1r; digfilt.b2r]

# More generally, the variables in the namespace digfilt are exposed by
# julia> digfilt. <hit tab>

end # module digfilt
