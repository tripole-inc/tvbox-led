# For cross compiling to ARM64 on a x86 machine running Ubuntu. Tripole 210105.
# Taken from "Practical Makefiles, by example" by John Tsiombikas
# http://nuclear.mutantstargoat.com/articles/make/#practical-makefile
# (More precisely, the section "A makefile for 99% of your programs".)

# Note: If you only make changes to def.h, do make clean then make.

# CC = gcc # For native ARM 64 build.
CC = aarch64-linux-gnu-gcc # For cross compile build on Ubuntu x86.
IDIR = .
CFLAGS = -Wall -O2 -I$(IDIR)

src = $(wildcard *.c)
obj = $(src:.c=.o)

tvbox-led: $(obj)
	$(CC) -o $@ $^ $(CFLAGS)

tvbox-led_static: $(obj)
	$(CC) -o $@ $^ $(CFLAGS) -static

.PHONY: doc
doc:
	doxygen Doxyfile

.PHONY: clean
clean:
	rm -f $(obj) tvbox-led

.PHONY: docclean
docclean:
	rm -fR html latex
