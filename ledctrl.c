/*!\file
    The functions here perform all the low level LED control functions, such as
    turning the LED on or off, blink it or do a ramp up-hold-down.
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/gpio.h>

#include "def.h"
#include "ledctrl.h"

extern int debug_level;

/*!
    The basic LED control function; turn the system LED on or off. The LED
    state is maintained by the GPIO handle.
*/
int led_onoff(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req, int onoff)
{

    /* We consider a LED on/off operation to be done instantaneously. */

    /**
    Remark: The LED is fully on or off here and dimmed operation is not
    implemented at this low level since the LED (i.e. the associated GPIO pin)
    will always be in an on or off state. However, dimmed operation can always
    be achived in a calling function by flickering the LED as in led_ramp()
    with a fixed duty cycle set somewhere between 0% and 100%.
    */

    led_data->values[0] = (onoff == 1) ? 1 : 0 ;

    int ret = ioctl(led_req.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, led_data);

    if (ret == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to issue "\
                            "GPIOHANDLE_SET_LINE_VALUES_IOCTL %d (%s)\n",
                            __func__, ret, strerror(errno));

        exit(1);
    }

    return 0;
}

/*!
    Makes the LED blink one or several times with 50% duty cycle.
*/
int led_blink(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req,
              int noblinks, int blink_onmusec, int onoff)
{
    int led_state1 = 1, led_state2 = 0,  worktime_musec = 0;

    if (onoff == 0) {
        led_state1 = 0;
        led_state2 = 1;
    }

    /* A blink is defined as an on-off (or off-on) operation with equal amounts
       of time on/off (i.e. 50% duty cycle). */

    /* On dimming: Dimmed operation can be achieved by replacing each of the
       led_onoff() calls below by a loop of such calls, each lasting only a
       short amount of time (e.g. 50ms for an on-off switching), cf. the
       flickering operation in led_ramp(). */

    for (int i = 1; i <= noblinks; i++) {
        led_onoff(led_data, led_req, led_state1);
        usleep(blink_onmusec);

        led_onoff(led_data, led_req, led_state2);
        usleep(blink_onmusec);
    }

    worktime_musec = 2*blink_onmusec*noblinks;

    return worktime_musec;
}

/*!
    Ramp the system LED on or off (linear increase/decrease of intensity).
*/
int led_ramp(struct gpiohandle_data *led_data,
             struct gpiohandle_request led_req, int onoff)
{
/**
    A ramp on (off) (i.e. increasing (decreasing) LED light intensity) has
    led_rampnumsteps, and each such step lasts let_ramptotmusec. During a
    step, the LED is on during the first portion of the step, which lasts
    led_onmicrosec, and the LED is off during the last portion, which lasts
    led_offmicrosec. All the time we have led_onmicrosec + led_offmicrosec =
    led_flickermusec, and from step to step led_onmicrosec is increased
    (decreased) with an amount led_quantmusec until the the LED is on (off)
    during an entire step, and the ramp process is terminated. One full ramp
    lasts let_ramptotmusec.
*/

    static const int led_rampnumsteps = LEDRAMPTIMEMISEC/LEDFLICKERMISEC;
    const int let_ramptotmusec = led_rampnumsteps *1000*LEDFLICKERMISEC;
    const int led_quantmusec = 1000*LEDFLICKERMISEC/led_rampnumsteps;

    int worktime_musec = 0;

    /* Not used but makes the compiler happy (initialized variables). */
    int led_onmusec = (onoff == 1) ? 0 : let_ramptotmusec;
    int led_offmusec = let_ramptotmusec -led_onmusec;

    for (int i=1; i<= led_rampnumsteps; i++) {
        if (onoff == 1) {
            led_onmusec = i*led_quantmusec;
            led_offmusec = (led_rampnumsteps -i)*led_quantmusec;
        } else {
            led_onmusec = (led_rampnumsteps -i)*led_quantmusec;
            led_offmusec = i*led_quantmusec;
        }

    if (led_onmusec > 0)
        led_onoff(led_data, led_req, 1);

    usleep(led_onmusec);

    if (led_offmusec > 0)
        led_onoff(led_data, led_req, 0);

    usleep(led_offmusec);
    }

    worktime_musec = let_ramptotmusec;

    return worktime_musec;
}

/*!
    This function performs an up-hold-down ramp with the LED. There is a hold
    period in the middle which can optionally be inverted. A typical use case
    for the function is to generate a smooth heartbeat-like pulsating light.
*/
int led_pulse(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req,
              int onoff, int inv_hold)
{
/**
    The standard behavior is a ramp-on (off), followed by a hold, followed
    by a ramp off (on). (Actions in parenthesis done when inv_flag == 1.)
    However, if inv_hold == 1 we do a blink instead of a hold in the middle.
*/

    int led_state1 = 1, led_state2 = 0, worktime_musec = 0;

    if (onoff == 0) {
        led_state1 = 0;
        led_state2 = 1;
    }

    int ramptime_musec = led_ramp(led_data, led_req, led_state1);
    worktime_musec += ramptime_musec;

    int holdtime_musec = 1000*LEDRAMPHOLDMISEC;

    if (inv_hold == 1) {
        led_blink(led_data, led_req, 1, holdtime_musec, led_state2);
    } else {
        usleep(holdtime_musec);
    }
    worktime_musec += holdtime_musec;

    ramptime_musec = led_ramp(led_data, led_req, led_state2);
    worktime_musec += ramptime_musec;

    return worktime_musec;
}
