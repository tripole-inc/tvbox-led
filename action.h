/*!\file
    Function declarations for the higher level LED blinking actions (tasks).
*/

#ifndef ACTION_H
#define ACTION_H

enum action_s;
struct gpiohandle_data;
struct gpiohandle_request;
struct netstat_data;

/*! Simple blink actions for the LED. */
int simple_action(struct gpiohandle_data *led_data,
                  struct gpiohandle_request led_req,
                  enum action_s smpl_act, int inv_flag);

/*! Indication of CPU load average. */
int cpuload_action(struct gpiohandle_data *led_data,
                   struct gpiohandle_request led_req,
                   int period_musec, enum action_s prg_acts, int inv_flag);

/*! Scans the status of the network interfaces and blink the result. */
int ifstate_action(struct gpiohandle_data *led_data,
                   struct gpiohandle_request led_req,
                   int period_musec, int inv_flag);

/*! Collects data for the network traffic intensity and blinks the result. */
int iftraffic_action(struct gpiohandle_data *led_data,
                     struct gpiohandle_request led_req,
                     struct netstat_data *if_traffic,
                     int period_musec, enum action_s prg_acts,
                     float *delay_tube, int force_lnkspeed,
                     char force_if, int inv_flag);

#endif
