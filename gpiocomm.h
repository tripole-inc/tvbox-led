/*!\file
    Functions for low level GPIO communication.
*/

#ifndef GPIOCOMM_H
#define GPIOCOMM_H

struct gpiohandle_data;
struct gpiohandle_request;

/*!
    Sets up/takes down a comms channel for a GPIO device via a character device.
*/
int gpio_updown(struct gpiohandle_data *gpioh_data,
                struct gpiohandle_request *gpioh_req,
                int gpioh_open);

/*! Gets GPIO chip name and line info. */
int gpio_getchipinfo(int fd_chrdev);

#endif
