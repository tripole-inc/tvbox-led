/*!\file
    The functions defined in this file perform the higher level actions of LED
    blinking, ranging from a single "alive" blink to bursts of blinks
    indicating load on one or several of the network interfaces.
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <linux/gpio.h>

#include "def.h"
#include "utility.h"
#include "ledctrl.h"
#include "getdata.h"
#include "action.h"

extern int debug_level;

/*!
    The LED blinks performed here are (variants of) a pulse, blink or burst.
    This functionality is only intended to act as an "alive" indicator.
*/
int simple_action(struct gpiohandle_data *led_data,
                  struct gpiohandle_request led_req,
                  enum action_s smpl_act, int inv_flag)
{
    int onoff = (inv_flag == 1) ? 0 : 1 ;
    int worktime_musec = 0;

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    switch (smpl_act) {
        case no_blnk: /* No blink, LED is completely off during the period. */
            led_onoff(led_data, led_req, inv_flag);
            usleep(1000*LEDONTIMEMISEC);
            worktime_musec = 1000*LEDONTIMEMISEC;
            break;
        case singl_blnk: /* Single (long) blink. */
            worktime_musec = led_blink(led_data, led_req, 1,
                                 1000*LEDONTIMEMISEC, onoff);
            break;
        case doubl_blnk: /* Double (short) blink. */
            worktime_musec = led_blink(led_data, led_req, 2,
                                 1000*LEDONTIMESHORTMISEC, onoff);
            break;
        case tripl_blnk: /* Triple-blink-burst (short blinks). */
            worktime_musec = led_blink(led_data, led_req, 3,
                                 1000*LEDONTIMESHORTMISEC, onoff);
            break;
        case pulse_blnk: /* Pulsating heartbeat with blink on the middle. */
            worktime_musec = led_pulse(led_data, led_req, onoff, 1);
            break;
        case pulse_hold: /* Pulsating heartbeat (with hold in the middle). */
            worktime_musec = led_pulse(led_data, led_req, onoff, 0);
    }

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    return worktime_musec;
}

/*! Indication of CPU load average by blink count and length of blibks. */
int cpuload_action(struct gpiohandle_data *led_data,
                   struct gpiohandle_request led_req,
                   int period_musec, enum action_s prg_acts, int inv_flag)
{
    static const int led_blinkonmusecmax = 1000*LEDONTIMEMISEC;
    static const int led_blinkonmusecmin = 500*LEDONTIMESHORTMISEC;

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    /* We use only the 1min average here (and since the data is already
       averaged there is little point in further averaging/filtering.*/
    float cpu_load = cpu_getload(); /* Can vary in interval 0-NUMBEROFCORES */

    /* From here on, use normalized load level in the interval [0,1]. */
    float load_level = trunc_to_unitintvl(cpu_load/(float)(NUMBEROFCORES));

    /* For better intelligibility of the blink sequences we can try nonlinear
       scaling of the load level (as in iftraffic_action()) but it seems that
       for CPU usage this this does improve intelligibility. */

    /* float loadlevel_scaled = myfcn("mysqrt", load_level); */

    float loadlevel_scaled = load_level;

    /* Higher load -> shorter blinks. */
    int led_blinkonmusec = loadlevel_scaled *led_blinkonmusecmin
                           +(1.0-loadlevel_scaled)*led_blinkonmusecmax;

    /* Each blink is followed by a quiet period, for better visibility. */
    float max_nblinks = (float) period_musec / (2*led_blinkonmusec);

    int nblinks = 0;
    if (load_level >= IDLELOADLVL)
        nblinks = max_nblinks*loadlevel_scaled;

    /* Do the actual blinking (if nblinks > 0). */
    int worktime_musec = 0, tot_nblinks_done = 0;

    if (nblinks > 0) {
        for (int j=1; j<= nblinks; j++) {
            led_blink(led_data, led_req, 1, led_blinkonmusec, inv_flag);
            tot_nblinks_done++;
            usleep(led_blinkonmusec);
            worktime_musec += 2*led_blinkonmusec;
        }
    } else {
        /* Do only 'alive' indication. */
        worktime_musec = simple_action(led_data, led_req, prg_acts, inv_flag);
    }

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    if (period_musec > worktime_musec)
        usleep(period_musec -worktime_musec);

    return 0;
}

/*!
    This is the simplest form of network status indication; the LED blinks
    only show if an interface is up or down.
*/
int ifstate_action(struct gpiohandle_data *led_data,
                   struct gpiohandle_request led_req,
                   int period_musec, int inv_flag)
{
    static const int blnk_ontime = 1000*LEDONTIMEMISEC;

    int worktime_musec = 0;

    int onoff = (inv_flag == 1) ? 0 : 1 ;

    int eth0_state = iface_getstate("eth0");
    int wlan0_state = iface_getstate("wlan0");

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    if (eth0_state + wlan0_state == 0) {
        worktime_musec = led_blink(led_data, led_req,
            mystr2num(LEDNUMBLINKSNOIFUP), blnk_ontime, onoff);
    } else if (eth0_state * wlan0_state > 0) {
        worktime_musec = led_blink(led_data, led_req,
            mystr2num(LEDNUMBLINKSBOTHIFUP), blnk_ontime, onoff);
    } else if (eth0_state >= 1) {
        worktime_musec = led_blink(led_data, led_req,
            mystr2num(LEDNUMBLINKSETH0IFUP), blnk_ontime, onoff);
    } else
        worktime_musec = led_blink(led_data, led_req,
            mystr2num(LEDNUMBLINKSWLAN0IFUP), blnk_ontime, onoff);

    if (period_musec > worktime_musec)
        usleep(period_musec -worktime_musec);

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

  return 0;
}

/*! 
    This function collects data on transferred bytes over the network
    interfaces and from this computes load averages. The result is then
    displayed by the system LED in the form of blink sequences.
*/
int iftraffic_action(struct gpiohandle_data *led_data,
                     struct gpiohandle_request led_req,
                     struct netstat_data *if_traffic,
                     int period_musec, enum action_s prg_acts,
                     float *delay_tube, int force_lnkspeed,
                     char force_if, int inv_flag)
{
/**
    This function has two loops, the measurement loop and the blink loop.
    In the outermost loop, the measurement loop, the network interface load
    is calculated. In the inner blink loop a number of blinks is performed
    corresponding to the value of the (accumulated, averaged) load.

    The motivation for this construction, and more generally the hierarchy of
    loops formed by these two and the outermost main loop in main.c, is the
    following: The key time scale appearing in the tasks performed by
    iftraffic_action() is the timescale for collecting network data, i.e. the
    time scale of the measurement loop: There is no reason to *adjust*
    anything else, such blink lengths, on any time scale shorter than the one
    on which data is acquired (even though the blink loop operates on a time
    scale shorter than that of the measurement loop). The time scale for the
    measurement loop should moreover not be too short since it will then not
    be meaningful or useful for visualization (and interpretation of the user). 
    For this reason we set the length of the measurement loop to minfitnblinks
    which is a (small) multiple of the longest blink we are going to use.
    This also means that one call to iftraffic_action() will in general not
    fit an integer number of traversals through the measurement loop. We solve
    this by simply cutting short (in general) the execution time for
    iftraffic_action() (which is period_musec nominally) and perform as many
    traversals as can fit in period_musec.
*/
  
    static const int led_blinkonmusecmax = 1000*LEDONTIMEMISEC;
    static const int led_blinkonmusecmin = 500*LEDONTIMESHORTMISEC;
    static const int led_blinkmusecmax = 2*led_blinkonmusecmax; /* 50% du-cy */

    /* Estimation of total computational delay (mostly file read/write) and
       work done in the present (and calling) function. */
    static const int comptime_musec = 1000; 

    /* This loop must be long enough to fit in a few long blinks. */
    static const int minfitnblinks = 2;
    static const int measlooptime_musec = minfitnblinks*led_blinkmusecmax;
    int measloop_numiter = (period_musec -comptime_musec) / measlooptime_musec;

    /* This variable represents the accumulated network interface load. */
    static float bucket_level = 0.0;

    /* These three are used for iface load calc. based on learned iface speed. */
    static int eth0_bpsmax = 0;
    static int wlan0_bpsmax = 0;
    static int nperiod = 1;

    int worktime_musec = 0, tot_nblinks_done = 0;

    struct netstat_data if_data;

    int eth0_rxbytesdiff, eth0_txbytesdiff;
    int wlan0_rxbytesdiff, wlan0_txbytesdiff;

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */
    
    /* Measurement loop. */
    for (int i=1; i<=measloop_numiter; i++) {

        /* Network tx/rx stats are collected (from proc file sys.) regardless
           of the state (up/down; see below) of the interfaces eth0, wlan0. */
        iface_getstats(&if_data);

        eth0_rxbytesdiff = if_data.eth0_rxbytes -if_traffic->eth0_rxbytes;
        eth0_txbytesdiff = if_data.eth0_txbytes -if_traffic->eth0_txbytes;
        wlan0_rxbytesdiff = if_data.wlan0_rxbytes -if_traffic->wlan0_rxbytes;
        wlan0_txbytesdiff = if_data.wlan0_txbytes -if_traffic->wlan0_txbytes;

        float measlooptime_sec = (float) measlooptime_musec / 1000000;
        /* Note: bits/s from here on. */
        int eth0_rxbpsavg = 8* eth0_rxbytesdiff /measlooptime_sec;
        int eth0_txbpsavg = 8* eth0_txbytesdiff /measlooptime_sec;
        int wlan0_rxbpsavg = 8* wlan0_rxbytesdiff /measlooptime_sec;
        int wlan0_txbpsavg = 8* wlan0_txbytesdiff /measlooptime_sec;

        *if_traffic = if_data;

    /**
        For the calculation of load over network interfaces to be done below,
        some form of reference level for traffic (expressed as e.g. bits/s)
        needs to be determined (cf. command line option -b). The reference level
        should define what a load value of 1.0 means. There are basically three
        ways of doing this. The first is to use the link speed as reported by
        the interface in question (eth0, wlan0) as a reference. This has the
        advantage of being simple but might give misleading results in cases
        where the effective bottleneck for the traffic lies somewhere else, such
        as in a router. As an alternative we can then use a fixed value (given
        as a command line option arg.) as a reference level. This is the
        recommended approach. A third way to determine the reference level is to
        learn, or rather guesstimate, a value for the reference level based on
        past traffic data. However, it is not straightforward to implement this
        in a way which is both simple and at the same time gives representative
        results. We have chosen to emphasize simplicity and therefore the
        learned reference level is obtained by a max operation (performed during
        each traversal of the measurement loop) combined with a slow forgetting
        of the max value.
    */

        nperiod++;
        nperiod = nperiod % BPSSUPPERIODLEN;

        /* Currently, the returned eth0 state is equiv. to linkspeed in Mbps. */
        int eth0_state = iface_getstate("eth0");
        float eth0_load = 0.0;

        if ((eth0_state > 0) && (force_if != 'w')) {

            if (nperiod == 0)
                eth0_bpsmax =
                    (int)((float) eth0_bpsmax*(100 -BPSFORGETPCT) / 100);

            int eth0_bps = 0;

            eth0_bps = (eth0_rxbpsavg > eth0_txbpsavg) ?
                        eth0_rxbpsavg : eth0_txbpsavg;

            if (eth0_bps > eth0_bpsmax)
                eth0_bpsmax = eth0_bps;

            /* This can happen e.g. by bad averaging at startup. */
            if (eth0_bpsmax > 1000 * 1000 * eth0_state)
                eth0_bpsmax = 1000 * 1000 * eth0_state;

            int eth0_speedbps = 0;

            switch (force_lnkspeed) {
                case 0: /* Link speed given by interface. */
                    eth0_speedbps = 1000 * 1000 * eth0_state;
                    break;
                case 1: /* Learned max speed. */
                    eth0_speedbps = eth0_bpsmax;
                    break;
                default: /* Link speed specified by cmd. line / config. file. */
                    eth0_speedbps = 1000 * 1000 * force_lnkspeed;
            }

            eth0_load = (float) eth0_bps / eth0_speedbps;

            /* E.g. with forced linkspeed the load can go outside of [0,1]. */
            eth0_load = trunc_to_unitintvl(eth0_load);

        }

        int wlan0_state = iface_getstate("wlan0");
        float wlan0_load = 0.0;

        if ((wlan0_state > 0) && (force_if != 'e')) {

            if (nperiod == 0)
                wlan0_bpsmax =
                    (int)((float) wlan0_bpsmax*(100 -BPSFORGETPCT) / 100);

            int wlan0_bps = 0;

            wlan0_bps = (wlan0_rxbpsavg > wlan0_txbpsavg) ?
                         wlan0_rxbpsavg : wlan0_txbpsavg;

            if (wlan0_bps > wlan0_bpsmax)
                wlan0_bpsmax = wlan0_bps;

            /* This can happen e.g. by bad averaging at startup. */
            if (wlan0_bpsmax > 1000 * 1000 * wlan0_state)
                wlan0_bpsmax = 1000 * 1000 * wlan0_state;

            int wlan0_speedbps = 0;

            switch (force_lnkspeed) {
                case 0: /* Link speed given by interface. */
                    wlan0_speedbps = 1000 * 1000 * wlan0_state;
                    break;
                case 1: /* Learned max speed. */
                    wlan0_speedbps = wlan0_bpsmax;
                    break;
                default: /* Link speed specified by cmd. line / config. file. */
                    wlan0_speedbps = 1000 * 1000 * force_lnkspeed;
            }

            wlan0_load = (float) wlan0_bps / wlan0_speedbps;

            /* E.g. with forced linkspeed the load can go outside of [0,1]. */
            wlan0_load = trunc_to_unitintvl(wlan0_load);

        }

        float the_load = (eth0_load > wlan0_load) ? eth0_load : wlan0_load ;

        for (int j=DELAYNUM; j>0; j--)
            delay_tube[j] = delay_tube[j-1];

    /**
        The number of blinks to be made (by the inner blink loop) during one
        iteration of the measurement loop is dependent on a state derived from
        load history. If this state is large, many blinks are issued, and if it
        is small few or no blinks are issued. The state is added to by recent
        load history and and it is subtracted from by the blinks issued. We
        use the metaphor of the state as a bucket (integrator) which is filled
        by an inflow consisting of a time average of recent load values and is
        emptied when blinks are issued. (The bucket also has a maximum level
        and will overflow for any inflow after it has reached the max. level.
        Similarly, it can not (persistently) underflow.) In all calculations
        below we use purely discrete time so a flow value also directly
        corresponds to an increase or decrease of bucket level during a unit
        (discrete) time interval.
    */

    /**
        The bucket inflow is filtered through a digital filter. The filter can
        be one of two types (set at compile time); a simple exponential
        averaging (one-pole filter) or a bi-quadratic filter with two zeros and
        two poles. (After tuning, there isn't a big difference in performance
        between the two but the latter can be made to provide a somewhat better
        transient response while retaining the good averaging properties of the
        former.)
    */

        static const char filt_type = FILTERTYPE; /* {'b', 'e'} */
        float bucket_inflow = 0.0;

        if (filt_type == 'b') { /* Bi quadradic filter. (Two poles, zeros.) */
            /* The biquad filter is here implemented using the so called second
               direct form which uses only one delay line for old values.
               This is accomplished by factoring the transfer function into a
               cascade of one all-pole filter followed by one all-zero filter.
               The delay line holds the values of the intermediate variable. */

            static const float a1 = FILT_A1, a2 = FILT_A2;
            static const float b0 = FILT_B0, b1 = FILT_B1, b2 = FILT_B2;

            delay_tube[0] = the_load -a1*delay_tube[1] -a2*delay_tube[2];
            bucket_inflow =
                   b0*delay_tube[0] +b1*delay_tube[1] +b2*delay_tube[2];

        } else { /* Exponential averaging. (One-pole filter.) */

            static const float avg_fact = AVG_FACT;
            float avgfact_sum = 0.0, avgfact_power = 1.0;

            delay_tube[0] = the_load;

            for (int j=0; j<DELAYNUM; j++) {
                bucket_inflow += avgfact_power *delay_tube[j];
                avgfact_sum += avgfact_power;
                avgfact_power *= avg_fact;
            }

            /* Normalize so that filter gain is unity for constant load. */
            bucket_inflow /= avgfact_sum;
        }

        bucket_level += bucket_inflow;

        /* The nonlinearity here is applied after filtering (of the flow). */
        bucket_level = trunc_to_unitintvl(bucket_level);

        /* Since we model this as a bucket; a version of Torricelli's law: */
        float bucketlevel_scaled = myfcn("mysqrt", bucket_level);

        /* Higher load -> shorter blinks. */
        int led_blinkonmusec = bucketlevel_scaled *led_blinkonmusecmin
                               +(1.0-bucketlevel_scaled)*led_blinkonmusecmax;

        /* Each blink is followed by a quiet period, for better visibility. */
        float max_nblinks = (float) measlooptime_musec / (2*led_blinkonmusec);

        int nblinks = max_nblinks*bucketlevel_scaled;

        /* Finally, time to do the actual blinking. */
        if (nblinks > 0) {
            for (int j=1; j<= nblinks; j++) {
                led_blink(led_data, led_req, 1, led_blinkonmusec, inv_flag);
                tot_nblinks_done++;
                usleep(led_blinkonmusec);
                worktime_musec += 2*led_blinkonmusec;
            }
        } else {
            usleep(measlooptime_musec -2*led_blinkonmusec);
        }

        /* Drain the bucket according to how many blinks were issued. */
        float nmloops_toempty_bucket = 2.5; /* Yes, we use a float here. */
        float bucket_outflow;
        bucket_outflow = nblinks/max_nblinks/nmloops_toempty_bucket;
        bucket_level -= bucket_outflow;

        if (debug_level > 1) {
            fprintf(stderr,
                    "%s: Info: Measurement period = %2.3f\n"\
                    "%s: Info: eth0_rxbps = %d, eth0_txbps = %d\n"\
                    "%s: Info: wlan0_rxbps = %d, wlan0_txbps = %d\n"\
                    "%s: Info: eth0_bpsmax = %d, wlan0_bpsmax = %d\n"\
                    "%s: Info: eth0_load = %1.3f, wlan0_load = %1.3f\n",
                    __func__, measlooptime_sec,
                    __func__, eth0_rxbpsavg, eth0_txbpsavg,
                    __func__, wlan0_rxbpsavg, wlan0_txbpsavg,
                    __func__, eth0_bpsmax, wlan0_bpsmax,
                    __func__, eth0_load, wlan0_load);
            if (eth0_load > wlan0_load) {
                fprintf(stderr, "%s: Info: iface load based on eth0 "\
                                "(force_linkspeed = %d)\n",
                                __func__, force_lnkspeed);
            } else {
                fprintf(stderr, "%s: Info: iface load based on wlan0 "\
                                "(force_linkspeed = %d)\n",
                                __func__, force_lnkspeed);
            }
            fprintf(stderr, "%s: Info: bucket_level = %1.3f, "\
                                      "bucketlevel_scaled = %1.3f\n"
                            "%s: Info: bucket_inflow = %1.3f, "\
                                      "bucket_outflow = %1.3f\n"\
                            "%s: Info: nblinks = %d, max_nblinks = %2.1f\n"\
                            "%s: Info: nperiod = %d (BPSSUPPERIODLEN = %d)\n",
                            __func__, bucket_level, bucketlevel_scaled,
                            __func__, bucket_inflow, bucket_outflow,
                            __func__, nblinks, max_nblinks,
                            __func__, nperiod, BPSSUPPERIODLEN);
        }

    } /* meas_loop */

    /* Do 'alive' indication. This might slide the schedule in the period a bit
       since we have possibly used up all time above (or at least used up too
       much time for a full indication, such as pulse, to fit in the remaining
       time to period_musec). However, it is anyway too cumbersome to time
       everything exactly since, for example, this means that we must in
       advance compensate for the time taken by simple_action() below, but this
       time depends on the value of prg_acts.  */
    if (tot_nblinks_done == 0) {
        int loc_worktime = simple_action(led_data, led_req, prg_acts, inv_flag);
        worktime_musec += loc_worktime;

        if (period_musec > worktime_musec)
            usleep(period_musec -worktime_musec);
    }

    led_onoff(led_data, led_req, inv_flag); /* Put LED in default state. */

    return 0;
}
