/*!\file
    The functions define here handle all forms of data collection; reading of
    config file, reading in the sysfs and proc file systems, and preprocessing
    of data obtained from there.
*/

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "def.h"
#include "getdata.h"

extern int debug_level;

/*! The cpu load average (total over all cores) is obtained by reading from a
    standard location in the proc file system. */
float cpu_getload(void)
{
    static const char loadavg_file[] = LOADAVGFILE;
    FILE *fd_loadavg = fopen(loadavg_file, "r");

    if (fd_loadavg == NULL) {
        if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to open stream %s (%s)",
                                __func__, loadavg_file, strerror(errno));

        exit(1);
    }

    /* The loadavg string is e.g. "0.25" or "5.25" i.e. 4 char +null byte. */
    char cmd_buf[5] = {'\0', '\0', '\0', '\0', '\0'};
    char *cmd_line = {'\0'};
    float cpu_loadavg = 0.0;

    cmd_line = fgets(cmd_buf, 5, fd_loadavg);

    if (cmd_line != NULL) {
       double onemin_loadavg = atof(cmd_line);
       cpu_loadavg = (float) onemin_loadavg;

       if (debug_level > 1)
           fprintf(stderr, "%s: Info: Read 1min cpu load (in [0 - num_cpus]) "\
                           "avg. as: %1.2f\n", __func__, cpu_loadavg);

    } else {
        if (debug_level > 1)
            fprintf(stderr, "%s: Warn: Could not read load average data "\
                            "from file %s \n", __func__, loadavg_file);
    }

    if (fclose(fd_loadavg) == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to close stream %s (%s)\n",
                            __func__, loadavg_file, strerror(errno));

        exit(1);
    }

    return cpu_loadavg;
}

/*!
   The state for a network interface (eth0/wlan0) is obtained by reading from
   a standard location in the sysfs system.
*/
int iface_getstate(const char *iface_name)
{

/**
    The following I have found out experimentally: The operating state is
    marked differently in sysfs for wired and wireless network interfaces.
    A wired interface, such as eth0, has opstate="up" as soon as the interface
    is configured (i.e. up) and there is a physical connection, even if it has
    no IP address assigned (and thus cannot pass traffic). On the other hand,
    a wireless interface, such as wlan0, has opstate="down" even when it is
    configured but has no IP address. A wireless interface has opstate="up"
    precisely when it (is up and) has an IP address.
*/
    /* First, read the operating state of the interface. */
    char *iface_opstate_file, *iface_linkspeed_file;

    if (strcmp(iface_name, "eth0") == 0) {
        iface_opstate_file = ETH0STATEPATH"operstate";
        iface_linkspeed_file = ETH0STATEPATH"speed";
    } else {
        iface_opstate_file = WLAN0STATEPATH"operstate";
        /* iface_linkspeed_file = WLAN0STATEPATH"speed"; */ /* Not valid. */
    }

    int fd_opstate = open(iface_opstate_file, O_RDONLY);

    if (fd_opstate == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to open %s (%s)",
                            __func__, iface_opstate_file, strerror(errno));

        exit(1);
    }

    /* iface is "up", "down" or "unknown", so we will use two char +null. */
    char state_str[3] = {'\0', '\0', '\0'};

    if (read(fd_opstate, state_str, 2) != 2) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to read %s (%s)",
                            __func__, iface_opstate_file, strerror(errno));

        exit(1);
    }

    if (close(fd_opstate) == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to close %s (%s).",
                            __func__, iface_opstate_file, strerror(errno));

        exit(1);
    }

    /* Both char arr in comparison here are two char +null (i.e. strings). */
    int state_num = (strcmp(state_str, "up") == 0) ? 1 : 0 ;

    /* Then, read the network interface link speed. (The return value to this
       here function is extd_statenum, which is synonymous with linkspeed). */
    int linkspeed = 0, extd_statenum = 0;

    if (state_num == 1) {

        if (strcmp(iface_name, "eth0") == 0) {

            int fd_linkspeed = open(iface_linkspeed_file, O_RDONLY);

            if (fd_linkspeed == -1) {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to open %s (%s)",
                                    __func__, iface_linkspeed_file,
                                    strerror(errno));

                exit(1);
            }

            /* The link speed string is "10\n", "1000\n" or "1000\n". */
            char linkspeed_str[5] = {'\0'};

            /* read() retrieves the char. sequence as-is (no null padding). */
            int num_char = read(fd_linkspeed, linkspeed_str, 5);

            if (num_char < 3) {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to read %s (%s)",
                                    __func__, iface_linkspeed_file,
                                    strerror(errno));

                exit(1);
            }

            if (close(fd_linkspeed) == -1) {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to close %s (%s).",
                                    __func__, iface_linkspeed_file,
                                    strerror(errno));

                exit(1);
            }

            const int linkspeed_arr[3] = {10, 100, 1000};
            linkspeed = linkspeed_arr[num_char -3]; /* Exclude trailing \n */

        } else { /* iface_name == "wlan0 " */

            char *wlan0_infocmd = WLAN0INFOCMD;

            /* Get rx, tx speeds. */
            FILE *fd_cmdbuf;
            if ((fd_cmdbuf = popen(wlan0_infocmd, "r")) == NULL) {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to open stream "\
                                    "for wlan0 speed info (%s)\n",
                                    __func__, strerror(errno));

                exit(1);
            }

            char cmd_buf[LINELEN];
            char *cmd_line;
            int linkspeed_rx = 0, linkspeed_tx = 0; /* Mbit/s */
            float linkspeed_rxf = 0.0, linkspeed_txf = 0.0;
            static const char *rx_pattern = "rx bitrate: ";
            static const char *tx_pattern = "tx bitrate: ";
            const int rxpattern_len = strlen(rx_pattern);
            const int txpattern_len = strlen(tx_pattern);
            char *rx_ptr, *tx_ptr, *rxrate_ptr, *txrate_ptr;

            int is_reading = 1;

            while (is_reading == 1) {

                cmd_line = fgets(cmd_buf, LINELEN, fd_cmdbuf);

                if (cmd_line != NULL) {

                    rx_ptr = strstr(cmd_buf, rx_pattern);
                    if (rx_ptr != NULL) {
                        rxrate_ptr = &rx_ptr[rxpattern_len-1];
                        sscanf(rxrate_ptr, "%f", &linkspeed_rxf);
                        linkspeed_rx = (int)(linkspeed_rxf);
                    }

                    tx_ptr = strstr(cmd_buf, tx_pattern);
                    if (tx_ptr != NULL) {
                        txrate_ptr = &tx_ptr[txpattern_len-1];
                        sscanf(txrate_ptr, "%f", &linkspeed_txf);
                        linkspeed_tx = (int)(linkspeed_txf);
                    }

                } else {
                    is_reading = 0;
                }
            }

            if (pclose(fd_cmdbuf) != 0) {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to execute wlan0 "\
                                    "speed info command successfully (%s)\n",
                                    __func__, strerror(errno));

                exit(1);
            }

            if (linkspeed_rx > linkspeed_tx) {
                linkspeed = linkspeed_rx;
            } else {
                linkspeed = linkspeed_tx;
            }

            if (debug_level > 1) {
                fprintf(stderr, "%s: Info: wlan0 "\
                                 "rx speed %d Mbit/s, tx speed %d Mbit/s\n",
                                __func__, linkspeed_rx, linkspeed_tx);
            }

        } /* if strcmp */
    }

    if (state_num == 1) {
        extd_statenum = linkspeed;
    }

    return extd_statenum;
}

/*!
    Network traffic statistics are computed based on data obtained from the
    proc file system.
*/
int iface_getstats(struct netstat_data *if_data)
{
    static const char netstats_file[] = NETSTATSFILE;
    static const char iface_names[NUMIFACENAMES][IFACENAMELEN] =
                                  {"eth0", "wlan0"};
    static const int num_ifacenames = NUMIFACENAMES;

    FILE *fd_netstats = fopen(netstats_file, "r");

    if (fd_netstats == NULL) {
        if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to open stream %s (%s)",
                                __func__, netstats_file, strerror(errno));

        exit(1);
    }

    char netstats_buf[BUFFERSIZE];
    char *netstats_line;

    /* The first two lines in netstats_file are just a header. */
    netstats_line = fgets(netstats_buf, BUFFERSIZE, fd_netstats);
    netstats_line = fgets(netstats_buf, BUFFERSIZE, fd_netstats);
    /* int line_count = 2; */

    /* Scan line by line and store only those for eth0, wlan0. */
    char iface_name[IFACENAMELEN] = {'\0'};     /* Init. with empty string. */
    char iface_namepadc[IFACENAMELEN] = {'\0'}; /* Init. with empty string. */

    long int eth0_rxbytes = -1, eth0_txbytes = -1;
    long int wlan0_rxbytes = -1, wlan0_txbytes = -1;

    long int iface_rxbytes, iface_txbytes;
    long int iface_rxpacks, iface_txpacks, dummy; /* Not used currently. */

    while (netstats_line != NULL) {
        netstats_line = fgets(netstats_buf, BUFFERSIZE, fd_netstats);
        /* line_count++; */

        sscanf(netstats_buf, "%s%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld",
               iface_name, &iface_rxbytes, &iface_rxpacks,
               &dummy, &dummy, &dummy, &dummy, &dummy, &dummy,
               &iface_txbytes, &iface_txpacks);

        int idx = 0, iface_idx = -1;

        while ((iface_idx == -1) && (idx < num_ifacenames)) {
            strcpy(iface_namepadc, iface_names[idx]);
            strcat(iface_namepadc, ":"); /* colons used in /proc/net/dev */
            iface_idx = (strcmp(iface_name, iface_namepadc) == 0) ? idx : -1 ;
            idx++;
        }

        switch (iface_idx) {
           case 0:
               eth0_rxbytes = iface_rxbytes;
               eth0_txbytes = iface_txbytes;
               break;
           case 1:
               wlan0_rxbytes = iface_rxbytes;
               wlan0_txbytes = iface_txbytes;
        }
    }

    if_data->eth0_rxbytes = eth0_rxbytes;
    if_data->eth0_txbytes = eth0_txbytes;
    if_data->wlan0_rxbytes = wlan0_rxbytes;
    if_data->wlan0_txbytes = wlan0_txbytes;

    if (debug_level > 1) {
        fprintf(stderr, "%s: Info: eth0_rxbytes = %ld, eth0_txbytes = %ld\n"\
                        "%s: Info: wlan0_rxbytes = %ld, wlan0_txbytes = %ld\n",
                        __func__, eth0_rxbytes, eth0_txbytes,
                        __func__, wlan0_rxbytes, wlan0_txbytes);
    }

    if (fclose(fd_netstats) == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to close stream %s (%s)\n",
                            __func__, netstats_file, strerror(errno));

        exit(1);
    }

    return 0;
}

/*!
   Disk utilization/throughput statistics are collected from the diskstats
   file in the proc file system.

   Note: This function is currently not used.
*/
int disk_getstats(struct diskstat_table *diskst_table)
{
    static const char diskstats_file[] = DISKSTATSFILE;
    static const char disk_names[NUMDISKNAMES][DISKNAMELEN] =
        {"mmcblk1", "mmcblk2", "sda", "sdb", "sdc", "sdd"};
    static const int num_disknames = NUMDISKNAMES;

    FILE *fd_diskstats = fopen(diskstats_file, "r");

    if (fd_diskstats == NULL) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to open stream %s (%s)",
                            __func__, diskstats_file, strerror(errno));

        exit(1);
    }
 
/**
    References for the stats listed in the fields in /proc/diskstats

    https://www.kernel.org/doc/Documentation/ABI/testing/procfs-diskstats

    https://www.kernel.org/doc/Documentation/iostats.txt

    Field names (for fields 1-14; the ones that are retrieved and used by us):
    "major_number", "minor_mumber", "device_name",
    "reads_completed_successfully", "reads_merged", "sectors_read",
    "time_spent_reading_millisec",  "writes_completed", "writes_merged",
    "sectors_written", "time_spent_writing_millisec",
    "IOs_currently_in_progress", "time_spent_doing_IOs_millisec",
    "weighted_time_spent_doing_IOs_millisec"
*/

    char diskstats_buf[BUFFERSIZE];
    char *diskstats_line = {'\0'};

    /* Scan line by line and store only those listed in disk_names */
    /* int line_count = 0; */

    char disk_name[DISKNAMELEN] = {'\0'};
    long int dummy, read_cds, read_mrg, read_sct, read_ts, write_cd;
    long int write_mrg, write_sct, write_ts, ios_inprg, time_sio, wtime_sio;

    struct diskstat_data diskst_data;
    diskst_data.read_cds  = -1;
    diskst_data.read_mrg  = -1;
    diskst_data.read_sct  = -1;
    diskst_data.read_ts   = -1;
    diskst_data.write_cd  = -1;
    diskst_data.write_mrg = -1;
    diskst_data.write_sct = -1;
    diskst_data.write_ts  = -1;
    diskst_data.ios_inprg = -1;
    diskst_data.time_sio  = -1;
    diskst_data.wtime_sio = -1;

    struct diskstat_data diskst_data_mmcblk1 = diskst_data;
    struct diskstat_data diskst_data_mmcblk2 = diskst_data;
    struct diskstat_data diskst_data_sda = diskst_data;
    struct diskstat_data diskst_data_sdb = diskst_data;
    struct diskstat_data diskst_data_sdc = diskst_data;
    struct diskstat_data diskst_data_sdd = diskst_data;

    while (diskstats_line != NULL) {
        diskstats_line = fgets(diskstats_buf, BUFFERSIZE, fd_diskstats);
        /* line_count++; */

        sscanf(diskstats_buf, "%ld%ld%s%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld",
               &dummy, &dummy, disk_name, &read_cds, &read_mrg, &read_sct,
               &read_ts, &write_cd, &write_mrg, &write_sct, &write_ts,
               &ios_inprg, &time_sio, &wtime_sio);

        diskst_data.read_cds  = read_cds;
        diskst_data.read_mrg  = read_mrg;
        diskst_data.read_sct  = read_sct;
        diskst_data.read_ts   = read_ts;
        diskst_data.write_cd  = write_cd;
        diskst_data.write_mrg = write_mrg;
        diskst_data.write_sct = write_sct;
        diskst_data.write_ts  = write_ts;
        diskst_data.ios_inprg = ios_inprg;
        diskst_data.time_sio  = time_sio;
        diskst_data.wtime_sio = wtime_sio;

        int idx = 0, disk_idx = -1;

        while ((disk_idx == -1) && (idx < num_disknames)) {
            disk_idx = (strcmp(disk_name, disk_names[idx]) == 0) ? idx : -1 ;
            idx++;
        }

        switch (disk_idx) {
           case 0:
               diskst_data_mmcblk1 = diskst_data;
               break;
           case 1:
               diskst_data_mmcblk2 = diskst_data;
               break;
           case 2:
               diskst_data_sda = diskst_data;
               break;
           case 3:
               diskst_data_sdb = diskst_data;
               break;
           case 4:
               diskst_data_sdc = diskst_data;
               break;
           case 5:
               diskst_data_sdd = diskst_data;
        }
    }

    diskst_table->mmcblk1 = diskst_data_mmcblk1;
    diskst_table->mmcblk2 = diskst_data_mmcblk2;
    diskst_table->sda = diskst_data_sda;
    diskst_table->sdb = diskst_data_sdb;
    diskst_table->sdc = diskst_data_sdc;
    diskst_table->sdd = diskst_data_sdd;

    if (debug_level > 1) {
        fprintf(stderr,
                "%s: Info: mmcblk1 : "\
                "read_cds = %ld, read_mrg = %ld, read_sct = %ld\n"\
                "%s: Info: mmcblk1 : "\
                "read_ts = %ld, write_cd = %ld, write_mrg = %ld\n"\
                "%s: Info: mmcblk1 : "\
                "write_sct = %ld, write_ts = %ld, ios_inprg = %ld\n"\
                "%s: Info: mmcblk1 : time_sio = %ld, wtime_sio = %ld\n",
                 __func__, diskst_data_mmcblk1.read_cds,
                 diskst_data_mmcblk1.read_mrg,
                 diskst_data_mmcblk1.read_sct,
                 __func__, diskst_data_mmcblk1.read_ts,
                 diskst_data_mmcblk1.write_cd, diskst_data_mmcblk1.write_mrg,
                 __func__, 
                 diskst_data_mmcblk1.write_sct, diskst_data_mmcblk1.write_ts,
                 diskst_data_mmcblk1.ios_inprg,
                 __func__, diskst_data_mmcblk1.time_sio,
                 diskst_data_mmcblk1.wtime_sio);
    }

    if (fclose(fd_diskstats) == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to close stream %s (%s)\n",
                            __func__, diskstats_file, strerror(errno));

        exit(1);
    }

    return 0;
}

/*!
    The config file is located, and if it exists it is read and preparsed.
    The result packaged together with the calling program name and returned.
*/
int config_read(const char *conf_file, const char *prg_name,
                    char* argv[], char *v_arg, char **vargp)
{
    int c_arg = 0;

    if (access(conf_file, R_OK) == 0) {
        if (debug_level > 1) {
                fprintf(stderr, "%s: Info: Found (readable) config file %s:\n",
                                __func__, conf_file);
        }

        FILE *fd_cfile = fopen(conf_file, "r");

        if (fd_cfile == NULL) {
            if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to open stream %s (%s)",
                                    __func__, conf_file, strerror(errno));

            exit(1);
        }

        /*  The config file is (first) read into a (preallocated) array of
            char of length MAXARGWORDS*MAXARGSTRLEN in the form of MAXARGWORDS
            consecutive slots (words) of length MAXARGSTRLEN. Each slot is
            intiated with null chars. Then, as long as there are command line
            argumentss left to process, each such argument is read into one
            slot. (A command line argument, or option, can be defined in at
            least wo ways, see below.) */

        for (int i=0; i<MAXARGWORDS; i++) {
            for (int j=0; j<MAXARGSTRLEN; j++) {
                v_arg[i*MAXARGSTRLEN+j] = '\0';
            }
        }

        int cstr_idx = -1, in_line_idx = 0, in_cstr_idx = 0;
        int space_num = 0, is_writing = 0;

        char cfile_buf[LINELEN] = {'\0'};
        char *cfile_line;

        /* fgets() appends a trailing null '\0' to the read stream/file. */
        cfile_line = fgets(cfile_buf, LINELEN, fd_cfile);

        /* A command line can be parsed in at least two ways, or modes: In both
           modes the first string on the command line which does not contain a
           space is the calling program name, so the modes only differ in the
           treatment of the following strings. In the first parsing mode, the
           'options' mode, any of the subsequent strings which is initiated by
           a dash "-" and ends just before another dash (or a newline) is
           considered as an option (string). In the second mode, the
           'arguments' mode, all strings of non-space characters (and thus
           ending just before a space or a newline) is considered as an
           argument. For example, in the 'options' mode the command line
           "tvbox-led -v 1 -p 2500" has three options, "tvbox-led", "-v 1" and
           "-p 2500". In the 'arguments' mode the same line has five arguments,
           "tvbox-led", "-v", "1", "-p" and "2500". Apparently, C's argv is
           supposed to be parsed in the 'arguments' mode. */

        static const int cmd_line_is_argsmode = 1;

        char conf_char;

        while ((in_line_idx < LINELEN) &&
                   (cfile_line[in_line_idx] != '\0') &&
                       (cfile_line[in_line_idx] != '\n')) {

            conf_char = cfile_line[in_line_idx];

            if (cmd_line_is_argsmode == 1) {  /* Using 'arguments' mode. */

                if (conf_char != ' ') {
                    if (is_writing == 0) {  /* Beginning of an argument. */
                        cstr_idx++;
                        in_cstr_idx = 0;
                        is_writing = 1;
                    } else {                /* Processing an argument. */
                        in_cstr_idx++;
                    }
                } else {
                    is_writing = 0;
                }

            } else {  /* Using 'options' parsing mode. */

                if (conf_char == '-') {    /* Found new substring. */
                    cstr_idx++;
                    in_cstr_idx = 0;
                    space_num = 0;
                    is_writing = 1;
                } else if (conf_char == ' ') {
                      if (space_num == 0) {  /* An opt. has exactly one space.*/
                           in_cstr_idx++;
                           space_num++;
                        } else {
                           is_writing = 0;
                       }
                } else {
                    in_cstr_idx++;
                }

            }

            if (is_writing == 1)
                v_arg[cstr_idx*MAXARGSTRLEN+in_cstr_idx] = conf_char;

            in_line_idx++;
        } /* while config parsing loop */

        c_arg = (cstr_idx > -1) ? cstr_idx +1 : 0 ;

        if (fclose(fd_cfile) == -1) {
            if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to close stream %s (%s)\n",
                                __func__, conf_file, strerror(errno));

            exit(1);
        }

        if (debug_level > 1) {
            fprintf(stderr, "%s: Info: Processed configuration file %s\n",
                            __func__, conf_file);
            fprintf(stderr, "%s: Info: Found %d option substrings.\n",
                            __func__, c_arg);

            char sub_str[MAXARGSTRLEN] = {'\0'};
            for (int i=0; i<c_arg; i++) {
                for (int j=0; j< MAXARGSTRLEN; j++) {
                    sub_str[j] = v_arg[i*MAXARGSTRLEN+j];
                }
                fprintf(stderr, "%s: Info: Substring num %d: %s\n",
                                __func__, i+1, sub_str);
            }
        }

        if (c_arg > 0) {
            vargp[0] = argv[0];
            for (int i=0; i<c_arg; i++)
                vargp[i+1] = &v_arg[i*MAXARGSTRLEN];

        }

    } else if (debug_level > 1) {
          fprintf(stderr, "%s: Info: Missing config file %s",
                           __func__, conf_file);
    }
    
    return c_arg;
}
