/*!\file
  This file contains almost all constants and macros used throughout the code.
  The only exceptions are a few constants that select/deselect experimental
  (or perhaps obsolete/redundant) features.
*/

#ifndef DEF_H
#define DEF_H

/* -------------------------------- Macros ----------------------------------*/

/* We all love stackoverflow. */
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/* ------------------------------- Constants ------------------------------- */

/*!
   The constants below (beginning with LED*) are defaults and can be adapted
   to your preference. The values of LEDONTIMEMISEC and LEDDFTPERIODMISEC
   define how long the red (system) LED is on/off during blink operation
   (starting from on (or off), when the system LED appears as dark red (or
   blue)). Typically, the value of LEDDFTPERIODMISEC is several times that
   of LEDONTIMEMISEC. During ramp LED operation, the intensity of the red
   LED is gradually increased (decreased) in steps over a period of
   LEDRAMPTIMEMISEC, by a type of pulse width modulation. In each such step,
   the LED does a flicker (quick on/off) with a period given by
   LEDFLICKERMISEC and in each flicker the LED is on during a first part of
   the flicker and off during the last. If the flicker is part of an on-ramp
   then the on-part of the flicker increases from step to step and if the
   flicker is part of an off-ramp the on-part decreases. (The value of
   LEDFLICKERMISEC should be chosen so that it corresponds to a flicker rate
   above, say. 25Hz. The individual flickers will then not be visible to the
   eye but will be perceived merely as an intensity change of the LED.) The
   nominal number of steps required to complete one ramp is
   LEDRAMPTIMEMISEC/LEDFLICKERMISEC but this relation is exact only when
   LEDRAMPTIMEMISEC is a multiple of LEDFLICKERMISEC. When doing an on
   (off) ramp followed by an off (on) ramp it is useful to have a holding
   period in the middle. The length of such a period is defined by
   LEDRAMPHOLDMISEC. Note: The prefix "mu" is micro- and "mi" is milli-.
*/
#define LEDONTIMEMISEC 180         /*!< {int} On-time for a normal blink.   */
#define LEDONTIMESHORTMISEC 60     /*!< {int} On-time for a short blink.    */
#define LEDMINPERIODMISEC 1500     /*!< {int} Min time for a program cycle. */
#define LEDDFTPERIODMISEC 4000     /*!< {int} Default time for a cycle.     */
#define LEDRAMPTIMEMISEC 600       /*!< {int} Time for a on/off ramp.       */
#define LEDFLICKERMISEC 30         /*!< {int} Oscillat. period at flicker.  */
#define LEDRAMPHOLDMISEC 200       /*!< {int} Hold time at "top" of blink.  */
#define LEDDEFAULTEXTERNSTATE 1    /*!< {0,1} On/off (on=red, off=blue).    */
#define LEDDEFAULTCYCLECODE 'h'    /*!< {chr} ascii value of a char         */
#define LEDNUMBLINKSNOIFUP "0"     /*!< {string} representing nonneg. int   */
#define LEDNUMBLINKSETH0IFUP "1"   /*!< {string} representing nonneg. int   */
#define LEDNUMBLINKSWLAN0IFUP "2"  /*!< {string} representing nonneg. int   */
#define LEDNUMBLINKSBOTHIFUP "3"   /*!< {string} representing nonneg. int   */

/*! Strings for LED cycle descript. msgs. (no lead/trail. space, max 75char). */
#define HEARTBEATPULSE "heartbeat pulse"                       /*!< {string} */
#define HEARTBEATPULSEBLINK "heartbeat pulse with blink"       /*!< {string} */
#define NOBLINKPULSE "LED off all the time"                    /*!< {string} */
#define SINGLEBLINKPULSE "single-blink"                        /*!< {string} */
#define DOUBLEBLINKBURSTPULSE "double-blink-burst"             /*!< {string} */
#define TRIPLEBLINKBURSTPULSE "triple-blink-burst"             /*!< {string} */
#define SYSTEMCPULOADSTATUS "system (CPU) load status"         /*!< {string} */
#define NETWORKINTERFACESTATUS "network interface status"      /*!< {string} */
#define HEARTBEATPULSENETSTATUS "network interface status "\
                                "+ alive-but-idle indication"  /*!< {string} */
#define NETWORKTRAFFIC "network traffic load "\
                                "+ alive-but-idle indication"  /*!< {string} */

/*! Strings used in help message and verbose output. */
#define BLINKMSG1 "     -> For the cycles n,m the number of blinks versus "\
                           "interface status is;\n"\
                  "        "LEDNUMBLINKSNOIFUP": no connection, "\
                            LEDNUMBLINKSETH0IFUP": eth0 up, "\
                            LEDNUMBLINKSWLAN0IFUP": wlan0 up, "\
                            LEDNUMBLINKSBOTHIFUP": both eth0 and "\
                                                  "wlan0 up\n" /*!< {string} */
#define BLINKMSG2 "     -> For the cycles L,m,N the alive indication is "\
                           "at every other period.\n"          /*!< {string} */
#define BLINKMSG3 "     -> If the option -c is missing only alive-but-idle "\
                           "indication is given.\n"            /*!< {string} */

/*! {int}
   Max number of args. in cmd. line, excl. calling cmd. name (max argc-1). */
/* Note: For example, the call "./tvbox-led -v 1 -p 2500" has 5 options*/
#define MAXARGWORDS 15

/*! {int}
  Max length of a command line argument +one null, e.g. 5 (for "2500"). */
#define MAXARGSTRLEN 7

/*! {string} Info msg. displayed with option -h (and invalid options). */
#define USAGEMSG "Usage (<i> is an integer and <c> is a character):\n %s "\
                       "[-h] [-v <i>] [-i] [-b <i>] [-r <c>] "\
                       "[-p <i>] [-a <c>] [-c <c>]\n"\
                 "Options (default values are used in place of missing or "\
                       "incorrect options):\n"\
                 "  -h Help (display this message)\n"\
                 "  -v Verbosity level, integer;\n"\
                 "     0: silent, 1: errors + warnings (default), "\
                                 "2: errors + warnings + info\n"\
                 "  -i Invert all blink patters in the program cycles below"\
                       " (LED on <-> LED off)\n"\
                 "  -b Bandwidth (Mbps) for eth0 and wlan0 "\
                       "load calculation, non neg. integer x;\n"\
                 "     x=0: iface link speed (default), "\
                       "x=1: learned max speed, x>1: set to x.\n"\
                 "  -r Restrict network load calc. to either of eth0, wlan0, "\
                       "single character; e,w\n"\
                 "  -p Period for one program cycle in millisecs., integ."\
                       " (default "STR(LEDDFTPERIODMISEC)\
                           ", min "STR(LEDMINPERIODMISEC)")\n"\
                 "  -a Alive-but-idle status indicator, "\
                           "single character;\n"\
                 "     h : "HEARTBEATPULSE" (default)\n"\
                 "     H : "HEARTBEATPULSEBLINK"\n"\
                 "     s : "SINGLEBLINKPULSE" (long blink)\n"\
                 "     d : "DOUBLEBLINKBURSTPULSE" (short blinks)\n"\
                 "     t : "TRIPLEBLINKBURSTPULSE" (short blinks)\n"\
                 "     q : "NOBLINKPULSE"\n"\
                 "  -c Code for system monitoring program cycle, single "\
                       "character;\n"\
                 "     L : "SYSTEMCPULOADSTATUS" (one minute average) \n"\
                 "     n : "NETWORKINTERFACESTATUS\
                           " (blink patterns for up/down of eth0, wlan0)\n"\
                 "     m : "HEARTBEATPULSENETSTATUS" (selected by -a)\n"\
                 "     N : "NETWORKTRAFFIC" (selected by -a)\n"

/*! {string} Info msg. displayed with option -h (and invalid options). */
#define SYNOPSISSTR "- Program for controlling the system LED on a TV-box "\
                    "to signal system status. -"

/*! {string} Info msg. added at start for verbosity_level > 1 */
#define STARTUPMSG "Finished startup, entered main (perpetual) loop.\n"\
                   "LED cycle "

/*! {string} Info msg. added at start for verbosity_level > 1 */
#define QUITINFOMSG "Press Ctrl-C (or send SIGINT) to interrupt and exit.\n"

/*! {string} Info msg. added at start for verbosity_level > 0 */
#define EXITINFOMSG "Press Ctrl-C (or send SIGINT) to exit.\n"

/*! This location is also printed with command line/config option -h */
#define CONFFILE "/usr/local/etc/tvbox-led.conf"

/*!
   Files in sysfs where the state of the network interfaces is monitored.
   (This may not be stable across kernel releases.) For more info, see
   https://www.kernel.org/doc/html/latest/admin-guide/sysfs-rules.html
*/
/*!
   Physical device file paths, for Ugoos AM6 plus:
   For kernel 5.10 and 5.13 the physical path for eth0 is
   /sys/devices/platform/soc/ff3f0000.ethernet/net/eth0/

   For kernel 5.10 the physical path for wlan0 is
   /sys/devices/platform/soc/ffe03000.sd/mmc_host/mmc0/\
   mmc0:0001/mmc0:0001:1/net/wlan0/

   For kernel 5.13 the physical path for wlan0 is
   /sys/devices/platform/soc/ffe03000.sd/mmc_host/mmc1/\
   mmc1\:0001/mmc1\:0001\:1/net/wlan0

   Virtual device file paths, for Ugoos AM6 plus (symlinks to the physical
   versions above):
   /sys/class/net/eth0/
   /sys/class/net/wlan0/

*/

#define ETH0STATEPATH "/sys/class/net/eth0/"              /*!< {string} */
#define WLAN0STATEPATH "/sys/class/net/wlan0/"            /*!< {string} */

/*! {string} The wlan0 link speed is based on data from this command. */
#define WLAN0INFOCMD "/usr/bin/iw dev wlan0 link"

#define DISKSTATSFILE "/proc/diskstats" /*!< {string} Source for disk stats. */
#define NETSTATSFILE "/proc/net/dev" /*!< {string} Source netw. traffic dat. */

#define DISKNAMELEN 10  /*!< {int} For various buffers holding disk names.   */
#define IFACENAMELEN 10 /*!< {int} For various buffers holding iface names.  */

#define LOADAVGFILE "/proc/loadavg" /*!< {string} Source for load avg. data.*/
#define NUMBEROFCORES 6             /*!< {int} Used for CPU load avg. calc. */
#define IDLELOADLVL 0.1 /*! {float} [0.0-1.0] Below this, do 'alive' indic. */

/* These need only be changed if additional network interfaces are added. */
#define NUMIFACENAMES 2 /*!< {int} See iface_getstats(). */
#define NUMDISKNAMES 6  /*!< {int} See disk_getstats(). */

#define BUFFERSIZE 300  /*!< {int} For reading network data from proc/       */
#define LINELEN 80      /*!< {int} Length of a line (of char.); for buffers. */

/*!
   There is a simple digital filter applied to the sequence of network load
   values before these are converted to blink sequences. Two compile time
   options are available; either a simple exponential weighting/averaging of
   values (the "exp" filter, a one-pole filter) or a slightly more advanced
   bi-quadratic (two poles, two zeros) filter (the "biq" filter). Both methods
   are followed by some nonlinear scaling and truncation and such before blinks
   are issued. (The parameter values for "exp" and "biq" are defined in-line,
   in action.c.)
*/
#define FILTERTYPE 'b'   /*!< {chr} Digital filter type "biq" or "exp".  */
#define DELAYNUM 3       /*!< {int} Delay line length for digital filter.   */

/*! Filter coefficients for the bi-quad filter, see iftraffic_action(). */
#define FILT_A1 -0.80902 /*! {float} Filter coefficient in the bi-quad filt. */
#define FILT_A2 0.25     /*! {float} Filter coefficient in the bi-quad filt. */
#define FILT_B0 0.36099  /*! {float} Filter coefficient in the bi-quad filt. */
#define FILT_B1 0.06555  /*! {float} Filter coefficient in the bi-quad filt. */
#define FILT_B2 0.01444  /*! {float} Filter coefficient in the bi-quad filt. */

/*! Averaging factor for the one-pole filter, see iftraffic_action(). */
#define AVG_FACT 0.4 /*! {float} Filter coefficient in the one-pole filter. */

/*! When the network link speed is learned (by cmd. line option) the following
    calculation is done: Every BPSSUPPERIODLEN period the last max recorded
    bps over a network interface is reduced with BPSFORGETPCT percent. (Since
    there is usually a 24h semi-cyclic variation of load it is reasonable to
    select BPSSUPPERIODLEN and BPSFORGETPCT such that (together with the value
    of the variable period_musec used in iftraffic_action()) the forgetting of
    the max value gets down to, say 0.5, over a 24h time span.) */
#define BPSSUPPERIODLEN 10000
#define BPSFORGETPCT 5  /*!< {int} 0-100 (Typically 3-10, or so.) */

/*!
   Normally, you should not need to change the two values GPIOCHIPDEV and
   GPIOLINENUM below. The red LED on the Ugoos AM6 is connected to a GPIO chip
   which appears as a character device under the name /dev/gpiochip1 (The chip
   is also present in sysfs at /sys/class/gpio/gpiochip412, under the name
   aobus-banks, with 15 lines starting at pin 412. The is LED connected to pin
   423 in aobus-banks, i.e. offset 11. The GPIO pin for the red LED on the AM6
   seems to be standard since the system LED is connected to GPIOAO_11 on
   generic s922x/s905x3 block diagrams.)
*/
#define GPIOCHIPDEV "/dev/gpiochip1" /* {string}  Chip 1 for Ugoos AM6.  */
#define GPIOLINENUM 11               /* {int}     Line 11 for Ugoos AM6. */
#define GPIOLINETAG "redled_gpio_ao_" /* {string} Descriptive label.     */

/*! Version info. */
#define PROGNAMESTR "\e[36mtvbox-led\e[39m\e[0m" /*!< {string} (w/ color) */
#define VERSIONSTR "AM6plus-tripole-220413"      /*!< {string} */

/* -------------------------------- Typedefs ------------------------------- */

/*! Used to select action in the simplest program cycle. */
enum action_s {
    no_blnk, singl_blnk, doubl_blnk, tripl_blnk, pulse_blnk, pulse_hold
};

/*! Used to compute network interface load. */
struct netstat_data {
    long int eth0_rxbytes, eth0_txbytes;
    long int wlan0_rxbytes, wlan0_txbytes;
};

/*! Not used currently. */
struct diskstat_data {
    long int read_cds, read_mrg, read_sct, read_ts, write_cd;
    long int write_mrg, write_sct, write_ts, ios_inprg, time_sio, wtime_sio;
};

/*! Not used currently. */
struct diskstat_table {
    struct diskstat_data mmcblk1;
    struct diskstat_data mmcblk2;
    struct diskstat_data sda;
    struct diskstat_data sdb;
    struct diskstat_data sdc;
    struct diskstat_data sdd;
};

#endif
