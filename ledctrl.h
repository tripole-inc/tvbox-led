/*!\file
    Functions to perform low level LED control, such as LED on/off or blink.
*/

#ifndef LEDCTRL_H
#define LEDCTRL_H

struct gpiohandle_data;
struct gpiohandle_request;

/*! Turns the system LED on or off. */
int led_onoff(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req, int onoff);

/*! Makes one or several on-off blinks of the system LED. */
int led_blink(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req,
              int noblinks, int blink_onmusec, int onoff);

/*! Ramps the system LED on or off. */
int led_ramp(struct gpiohandle_data *led_data,
             struct gpiohandle_request led_req, int onoff);

/*! Performs an up-hold-down ramp with the LED. */
int led_pulse(struct gpiohandle_data *led_data,
              struct gpiohandle_request led_req,
              int onoff, int inv_hold);

#endif
