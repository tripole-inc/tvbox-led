/*!\file
    Functionality to print general information about program execution.
*/

#ifndef MSG_H
#define MSG_H

/*! Prints out explanatory text at start of first main loop iteration. */
void firstloop_verbosemsg(int only_sact, char alive_char, char cycle_char);

#endif
