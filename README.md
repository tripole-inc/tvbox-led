# TVbox-LED

`TVbox-LED` is a program to enable use of the status LED as a system status and
activity indicator on a TV-box running Linux. This feature is standard under
Android but is often not available on the Linux distros for ARM that can be run
on TV-boxes. Currently, `TVbox-LED` can indicate 'alive' status by blinks(s) or
heartbeat pulse, signal network interface status up/down (eth0, wlan0) or show
network activity (intensity) or CPU load by blink sequences with varying duty
cycle. The code is written for Ugoos AM6 plus but it should be easy to adapt it
to other boxes as well. (A download link for binaries is given at the end of
the document.)

Building
--------

The program can be built (e.g. by cross compilation) using a standard build
environment (including Linux kernel headers) to produce a dynamically linked
binary. A makefile is provided for this, as well as to produce html
documentation for the code.

The program can be built on Ubuntu 20.04 (Focal) x86 with the ARM cross
compiler `aarch64-linux-gnu-gcc` installed by doing

`$ make`

The result should be a binary named `tvbox-led` of about 46kB size.
To (optionally) build the html documentation, if doxygen and graphviz are
installed on the build host, do

`$ make doc`

The documentation will appear under the directory `html` (starting with
`html/index.html`).

(There is also a static build target in the makefile; the resulting binary
will be about 700kB.)

Usage
-----

`TVbox-LED` can be run from command line, e.g. like

`# ./tvbox-led -a h -p 3500 -c N`

The options can also be put in a config file, at a location specified at
compile time (see def.h). (The options should then be put on a single line,
just as the command line argument string `"-a h -p 3500 -c N"` in the example
here. The available options are listed in the command line help menu shown
below.)

The command line help menu for `TVbox-LED` looks like this:

```
# ./bin/tvbox-led -h
This is tvbox-led, version AM6plus-tripole-220413 (binary name tvbox-led).
- Program for controlling the system LED on a TV-box to signal system status. -
Location of (optional) config file: /usr/local/etc/tvbox-led.conf
If no options are given, this file (if it exists) will be parsed for options.
Usage (<i> is an integer and <c> is a character):
 tvbox-led [-h] [-v <i>] [-i] [-b <i>] [-r <c>] [-p <i>] [-a <c>] [-c <c>]
Options (default values are used in place of missing or incorrect options):
  -h Help (display this message)
  -v Verbosity level, integer;
     0: silent, 1: errors + warnings (default), 2: errors + warnings + info
  -i Invert all blink patters in the program cycles below (LED on <-> LED off)
  -b Bandwidth (Mbps) for eth0 and wlan0 load calculation, non neg. integer x;
     x=0: iface link speed (default), x=1: learned max speed, x>1: set to x.
  -r Restrict network load calc. to either of eth0, wlan0, single character; e,w
  -p Period for one program cycle in millisecs., integ. (default 4000, min 1500)
  -a Alive-but-idle status indicator, single character;
     h : heartbeat pulse (default)
     H : heartbeat pulse with blink
     s : single-blink (long blink)
     d : double-blink-burst (short blinks)
     t : triple-blink-burst (short blinks)
     q : LED off all the time
  -c Code for system monitoring program cycle, single character;
     L : system (CPU) load status (one minute average)
     n : network interface status (blink patterns for up/down of eth0, wlan0)
     m : network interface status + alive-but-idle indication (selected by -a)
     N : network traffic load + alive-but-idle indication (selected by -a)
     -> For the cycles n,m the number of blinks versus interface status is;
        0: no connection, 1: eth0 up, 2: wlan0 up, 3: both eth0 and wlan0 up
     -> For the cycles L,m,N the alive indication is at every other period.
     -> If the option -c is missing only alive-but-idle indication is given.
```

*On privileges:* tvbox-led must be run as root, or at least with enough
privileges, to have access to the GPIO device `/dev/gpiochipX` where `X` is 1
for Ugoos AM6 plus.

*On systemd:* There is a template available for a systemd service definition
file so that the binary `tvbox-led` can be started and managed by systemd.

*On network load calculation:* Per default the program detects which of the
interfaces (eth0, wlan0) are up and uses the maximum bandwidth (link speed)
available from these as basis for the load (percentage) calculation. Often this
is not desirable however, since the effective bottleneck for the throughput to
some destination lies elsewhere, e.g. in a router or with the internet service
provider. For such situations the option -b is provided with which a reference
bandwidth (max network speed) can be specified which acts as basis for the load
calculation. In fact, this is the recommended approach. (There is also the
possibility of using the value 1 with option -b which will enable a simple
method for learning the max network speed by applying a (slow) forgetting
factor to the past max values, see iftraffic_action() in action.c).)

License
-------

The code for `TVbox-LED` is released under the Gnu General Public License v3.0,
see the file LICENSE.

Documentation
-------------

All parts of the code are documented with doxygen comments and a good overview
of the structure can be obtained by browsing the html docs which also include
call and caller graphs for the functions.

Additional features
-------------------

The code is modular and it should be straightforward to add features or
modifying the behavior of the program. (Some suggestions for this are listed at
the top of main.c along with a ToDo list.) In particular, adding functionality
so that storage i/o load (for SD/USB/emmc) can be monitored and displayed is
desirable. (Work for this has begin with a function disk_getstats() in
getdata.c)

LED control
-----------

At the time of writing of this program the relevant nodes for the system LED
had not yet been introduced in dts (device tree source) file for the Ugoos AM6
plus in the Linux kernel source tree (and the available documentation on the
Ugoos was very sparse). Therefore, no attempt was made to try to use the
functionality of the LED subsystem of the Linux kernel. Instead, the method
chosen for control of the status LED is direct bit banging of the GPIO pin that
turns on or off (completely) the (red part of the) system LED. (More info about
the system LED on the Ugoos can be found e.g. in def.h and main.c)

GPIO
----

In order to run the code on other boxes than the Ugoos AM6 plus, the correct
GPIO pin for the LED needs to be located (cf. the GPIO-related constant
definitions in def.h and the remarks about GPIO in main.c). The GPIO pins can
be investigated and identified with the command `gpiodetect` (in the
`gpio-utils` package on Manjaro ARM linux and in the package `gpiod` on Debian).

Hardware example: On the Ugoos AM6 plus the GPIO "chips" (i.e. hardware
functionality in the s922x SoC) can be accessed via two character devices and
the chips are also visible in the sysfs system.

If one runs the command (as root)
```
# for f in `ls -d /sys/class/gpio/gpiochip*`; \
do echo $f `cat $f/label $f/base $f/ngpio` ; done  
```
the output on the Ugoos AM6 plus is
```
/sys/class/gpio/gpiochip412 aobus-banks 412 15
/sys/class/gpio/gpiochip427 periphs-banks 427 85
```
The (red part of the) system LED is connected to a line on the first bank above,
labeled aobus-banks (with 15 lines). The line number offset for the LED is 11
(i.e.\ line 423). The GPIO chip aobus-banks also appears as a character device
at `/dev/gpiochip1`. (The device name is listed as `gpiochip1` in the directory
`/sys/class/gpio/gpiochip412/device`.)

Download
--------

Compiled binaries of `TVbox-led` can be found
[here](https://mega.nz/folder/zNVRGYaI#yFLVBHCYs7IAWURdNTsmHQ).

Disclaimer
----------

The code runs well on my Ugoos AM6 plus but I have no idea about your box (it
might even get damaged). Use at your own risk!
