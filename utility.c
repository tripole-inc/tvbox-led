/*!\file
    The functions here perform certain simple auxiliary tasks, such as linear
    interpolation of a tabulated function.
*/

#include <string.h>

#include "utility.h"

/*! A very simple number-to-string converter. */
int mystr2num(char *numstr)
{
    int resnum = 0, len = strlen(numstr);

    for (int i=0; i<len; i++)
        resnum = resnum *10 +(numstr[i] -'0');

    return resnum;
}

/*! Truncate values to [0,1]. */
float trunc_to_unitintvl(float x)
{
    float ret;

    if (x < 0.0) {
        ret = 0.0;
    } else if (x > 1.0) {
        ret = 1.0;
    } else {
        ret = x;
    }

    return ret;
}

/*!
    Computes tabularized functions on [0,1] using linear interpolation.
    (Currently, only mysqrt (coarse square root) implemented.
*/
float myfcn(const char *fcn_name, float x)
{
    float fcn_val;

    /* Table for a crude version of sqrt() restricted to [0,1]. */
    const float mysqrt_args[9] =
        {0.0, 0.01, 0.02, 0.05, 0.1, 0.2, 0.4, 0.8, 1.0};
    const float mysqrt_vals[9] =
        {0.0, 0.1, 0.14, 0.22, 0.32, 0.45, 0.63, 0.89, 1.0};

    /* Table for the identity fcn. on [0,1]. */
    const float id01_args[2] = {0.0, 1.0};
    const float id01_vals[2] = {0.0, 1.0};

    const float *fcn_args = id01_args;
    const float *fcn_vals = id01_vals;

    /* Add your own functions here (and above). (x-vals. must include 0,1.) */
    if (strcmp(fcn_name, "mysqrt") == 0) {
        fcn_args = mysqrt_args;
        fcn_vals = mysqrt_vals;
    }

    /* Simple linear interpolation between values in [0,1] (trunc. outside). */
    x = trunc_to_unitintvl(x);

    int i = 1;
    while (x > fcn_args[i]) {
        i++;
    }

    float xfrac = (x -fcn_args[i-1])/(fcn_args[i] -fcn_args[i-1]);
    fcn_val = (1.0 -xfrac)*fcn_vals[i-1] +xfrac*fcn_vals[i];

    return fcn_val;
}
