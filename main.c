/*!\file

tvbox-led
---------

By Tripole, 2021-02-20.

With tvbox-led you can control the system LED on an 'Ugoos AM6 plus' TV-box
running Linux and use the LED to indicate various aspects of system status.
It comes with a simple mode where the LED is only used to indicate "alive" and
a couple of more advanced modes where the network interface status or load is
indicated by the blinking activity of the LED.

This program was designed for the Ugoos AM6 plus but it should be easy to adapt
it to other boxes as well (once the LED GPIOs are known; see e.g. libgpiod for
tools to probe/detect the relevant GPIOs for your box). The code is also
modular and it should not be difficult to extend it and add new functionality
and blinking modes.

The system LED on the Ugoos AM6 box is in fact three different LEDs fused into
one aperture, a red, a blue and a white LED. When booting into Linux the blue
LED is constantly on and the red LED is initially on as well, together giving
a pink(ish)-red color. This program controls the behavior of the red LED. By
turning it off or on, a blue or red color can be created, or something in
between (by modulation). The LED is controlled via the GPIO chardev interface.

The code is released under the Gnu General Public License v3.0.

Notes:

- How to compile:
I don't know the exact dependencies required, but the Linux kernel source and a
standard build environment should work (see e.g. Debian/Ubuntu documentation).
In order to (cross) compile a dynamically linked binary, simply run 'make' in
the base directory (or run 'aarch64-linux-gnu-gcc *.c -o tvbox-led').
The result is an aarch64 binary named 'tvbox-led'.

- Documentation: If you have doxygen and graphviz installed you can generate
  nice html documentation with 'make doc' (the starting point in the doc tree
  will then be the file html/index.html).

- tvbox-led must be as root (e.g. since /dev/gpiochipX needs to be manipulated).

- The code here uses the Linux character device GPIO API (v1), a.k.a. GPIOLIB.
A good introduction to this is given in the presentation "GPIO for Engineers
and Makers" by Linus Walleij, Linaro, presented at Linaro Connect 2016;
https://elinux.org/images/9/9b/GPIO_for_Engineers_and_Makers.pdf
Another source of info is the Linux kernel file tools/gpio/gpio-utils.c
An alternative way to access the LED is via /sys/class/gpio in the Linux sysfs
interface, which is straightforward, but this GPIO interface is depricated.
A third option, with some limitations, is to use the functionality provided
by the gpio-leds driver (module) in the Linux kernel. This however requires
certain entries in the device tree which are currently not present in the
(mainline) dts for the Ugoos AM6 plus.

- Use at your own risk (it works well on my box but I have no idea about yours).

Limitations:

   - Can only handle (simultaneously) one wired and one wireless connection
     (eth0, wlan0).

On error handling and exit codes:

   - Any error which requires an exit (such as an error at open/close or
     read/write to file/stream), is exited with a generic code 1. However, the
     errno variable is most often used to form and print an informative message
     if debug_level > 0 (which is defined at compile time).

ToDo:
   - Cleanup code; use fopen/fclose where possible etc., allow for continued
     executing when such an operation fails (i.e. allow graceful degradation).

   - Add monitoring of mmc/usb/ext disk i/o. (Started; see disk_getstats().)

   - Add functionality for reading and using other variables from e.g. sysfs.

   - Scriptability: Run external command in a shell (e.g. via system()) and
     "blink" the exit code. 
     + The return code can also be used to trigger combinations of functions,
       such as blink patters, or change of blink cycle program. This way,
       tvbox-led becomes only the 'display' layer of more elaborate monitoring
       tools (which can leverage the scripting capabilities of a shell).

   - Modularize the design further.

   - Implement and utilize also dimmed operation of the LED into the blinking
     patterns. (Dimming can be done by duty cycle variation when the LED is
     operated in flickering mode, as in led_ramp(), cf. comment in led_onoff().)

   - Add support for /sys/class/led (not yet enabled in .dtb for Ugoos AM6). 

   - Set up version to be linked to GitLab tag.

You are cordially invited to take part in the above, by forking or pull
requests (preferrably the former).

Gotchas:

   - I could not get a static float array, declared locally in a function, to
     survive properly between function calls (with the given gcc compiler and
     options as in Makefile). (A static float works fine, however.)
*/

/* --------------------------- C/Linux Includes ---------------------------- */

#include <stdlib.h>       /* Basic programming functionality, like exit().  */
#include <fcntl.h>        /* File handling (like open/close) of files.      */
#include <unistd.h>       /* Basic file read/write (like sysfs files).      */
#include <stdio.h>        /* stdin, stdout, stderr, and the fprint() fam.   */
#include <string.h>       /* String handling functions, like strcpy() etc.  */
#include <errno.h>        /* Defines errno and gives explicit error codes.  */
#include <sys/ioctl.h>    /* For ioctl etc.                                 */
#include <signal.h>       /* For interrupt signals (e.g. Ctrl-C (SIGINT)).  */
#include <getopt.h>       /* getopt(), optarg and opterr  */
#include <libgen.h>       /* For the basename function.   */
#include <linux/gpio.h>   /* Basic Linux GPIO stuff.  */
#include <linux/stddef.h> /* The macro for NULL       */

/* ---------------------------- Local includes ----------------------------- */

#include "def.h"      /* Constants and macro definitions.   */
#include "utility.h"  /* Utility functions for computation. */
#include "ledctrl.h"  /* Low level LED control (on/off, blink etc.).       */
#include "gpiocomm.h" /* Low level comm. with GPIO (e.g. up/down handle).  */
#include "getdata.h"  /* Data gathering (e.g. reading of proc/, sysfs/).   */
#include "action.h"   /* Higher level (blink sequence) actions. */
#include "msg.h"      /* Startup message.  */

/* -------------------------------- Globals -------------------------------- */

int debug_level = 0; /*!< {0,1,2} When debug_level=0 verbosity takes over. */
int verbosity_level = 1; /*!< {0,1,2} Used at command line and in config.  */
int exit_signal = 0; /*!< {0} Used in the signal handler. */

/*! opterr is initialized to 0 in main() (subtle point, see getopt.h). */
extern int opterr;

/* extern int errno; (Should not be explicitly declared, see man errno(3).)  */

/* -------------------------- In-module functions -------------------------- */

/*! A simple (but POSIX compliant, no less) interrupt signal handler. */
static void sig_handler(int sig_num){
    if (sig_num == SIGHUP || sig_num == SIGINT || sig_num == SIGTERM)
        exit_signal = sig_num; /* List signals/numbers with $kill -l */
}

static void init_signals(struct sigaction sigact){
    sigact.sa_handler = sig_handler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigaction(SIGHUP, &sigact, (struct sigaction *)NULL);
    sigaction(SIGINT, &sigact, (struct sigaction *)NULL);
    sigaction(SIGTERM, &sigact, (struct sigaction *)NULL);
}

/* --------------------------------- Main ---------------------------------- */

/*!

    The general workflow in main() is simple:

    1. Parse arguments from command line or config file (using config_read()
       and getopt()).

    2. Set up GPIO communication (using gpio_updown()).

    3. Enter perpetual loop depending on selected cycle (program), interrupted
       only by a change in exit_signal (detected by sig_handler()).
       (The cycle actions are then executed by one of simple_action(),
       ifstate_action() or iftraffic_action().)

The overall logic for arguments (options) to tvbox-led is as follows:
If there are command line options, use these. Otherwise try to read the config
file, and if options are found use these. As a last resort, use default values
(from def.h).

*/

int main(int argc, char *argv[])
{
    int ret = -1;

    /* The following defaults are used in various places of the code below. */
    char led_cycle = LEDDEFAULTCYCLECODE;
    char alive_indic = 'h';   /* Default value for the 'alive' indication. */
    char forced_iface = '\0'; /* Let the iface w/ max load determine load. */
    int invert_flag = 0;      /* By default, don't invert blink patterns. */
    int forced_linkspeed = 0; /* Let net capacity be given by iface speed. */
    int simple_progact = 1;   /* Let a simple program action be default. */
    int period_musec = 1000*LEDDFTPERIODMISEC; /* Default for prog. cycle. */
    enum action_s prg_acts = pulse_hold; /* Default 'alive' indicator type. */
    enum action_s loc_prgacts = no_blnk; /* Used only in iftraffic_action(). */

    /* Set up elementary interrupt signal handling. */
    struct sigaction led_sigact;
    init_signals(led_sigact);
    
    /* Prepare for argument (options) parsing. (An option consists of a switch
       (e.g '-p') followed by a value (e.g. '2000'), i.e. two arguments.) */
    int c_arg = 0;
    char v_arg[MAXARGWORDS*MAXARGSTRLEN] = {'\0'};

    char prog_name[LINELEN] = {'\0'};
    strcpy(prog_name, basename(argv[0]));
    int progname_len = strlen(prog_name);

    int p_argc = 0;
    char **p_argv;
    p_argv = calloc(1, (progname_len+MAXARGWORDS)*sizeof(char *));

    const char config_file[] = CONFFILE;

    char opts_str[LINELEN] = {'\0'};

    if (argc == 1)  /* No cmd. line arguments: We need to try to read config. */
        c_arg = config_read(config_file, prog_name, argv, v_arg, &*p_argv);

    if (c_arg > 0) {    /* We found a config file with at least one argument. */
        p_argc = c_arg +1; /* c_arg doesn't include the calling prog. name. */
        for (int i=1; i<p_argc; i++) {
           strcat(opts_str, p_argv[i]);
           strcat(opts_str, " ");
        }
    } else {
        p_argc = argc;
        p_argv = argv;
    }

    /* Do options parsing, using arguments from command line or config file. */
    opterr = 0;

    int opt;

    char usage_msg[40*LINELEN] = USAGEMSG;

    while ((opt = getopt(p_argc, p_argv, "hv:ib:r:p:a:c:")) != -1)
        switch (opt) {
            case 'v':
                verbosity_level = mystr2num(&optarg[0]);
                break;
            case 'i':
                invert_flag = 1;
                break;
            case 'b':
                forced_linkspeed = mystr2num(&optarg[0]);
                break;
            case 'r':
                forced_iface = optarg[0];
                break;
            case 'p':
                period_musec = 1000*mystr2num(&optarg[0]);
                if (period_musec < 1000*LEDMINPERIODMISEC)
                     period_musec = 1000*LEDDFTPERIODMISEC;

                break;
            case 'a':
                alive_indic = optarg[0];
                break;
            case 'c':
                led_cycle = optarg[0];
                simple_progact = 0;
                break;
            case '?':
            case 'h':
                fprintf(stderr, "This is %s, version %s (binary name %s).\n",
                                PROGNAMESTR, VERSIONSTR, prog_name);
                fprintf(stderr, "%s\n", SYNOPSISSTR);
                fprintf(stderr, "Location of (optional) config file: %s\n",
                                CONFFILE);
                fprintf(stderr, "If no options are given, this file (if "\
                                "it exists) will be parsed for options.\n");

                strcat(usage_msg, BLINKMSG1);
                strcat(usage_msg, BLINKMSG2);
                strcat(usage_msg, BLINKMSG3);
                fprintf(stderr, usage_msg, prog_name);

                exit(0);
                break;
            default:
                fprintf(stderr,"We should never end up here.");
        }

    /* Single out the alive-indicator; it is used in several cycle programs. */
    switch (alive_indic) {
        case 'h':
            prg_acts = pulse_hold;
            break;
        case 'H':
            prg_acts = pulse_blnk;
            break;
        case 'q':
            prg_acts = no_blnk;
            break;
        case 's':
            prg_acts = singl_blnk;
            break;
        case 'd':
            prg_acts = doubl_blnk;
            break;
        case 't':
            prg_acts = tripl_blnk;
            break;
    }

    if (debug_level > 0) {
        fprintf(stderr, "%s: Info: compiled with debug_level = %d > 0; "\
                        "verbosity set to 2.\n",
                        __func__, debug_level);
        verbosity_level = 2;
    }

    /* This is only information but it is important so we print it anyway. */
    if (verbosity_level > 0) {
        if (verbosity_level > 1) {
                char cmd_line[LINELEN] = {'\0'};
                for (int i=0; i<argc; i++) {
                    strcat(cmd_line, argv[i]);
                    strcat(cmd_line, " ");
                }
            if (c_arg > 0) {
                fprintf(stderr, "Program call (options from conf. file):\n"\
                                "     %s%s\n", cmd_line, opts_str);
            } else {
                fprintf(stderr, "Program call: \n     %s\n", cmd_line);
            }
        }
        fprintf(stderr, "Starting %s, process ID %d. ", prog_name, getpid());
        fprintf(stderr, EXITINFOMSG);
    }

    /* Set up request and get GPIO line for LED. */
    struct gpiohandle_request gh_req;
    struct gpiohandle_data gh_data;

    gpio_updown(&gh_data, &gh_req, 1);

    /*! Initialize the LED to off state. (This is the default state inside
       this code; outside the (assumed) state is LEDDEFAULTEXTERNSTATE.) */
    led_onoff(&gh_data, gh_req, 0);

    if (verbosity_level > 1)
        firstloop_verbosemsg(simple_progact, alive_indic, led_cycle);

    struct netstat_data if_nsdata;
    if_nsdata.eth0_rxbytes = if_nsdata.eth0_txbytes = 0;
    if_nsdata.wlan0_rxbytes = if_nsdata.wlan0_txbytes = 0;

    int wtime_musec = 0;
    float delay_line[DELAYNUM]; /* For filtering of data (cpu/net/disk) vals. */
    for (int i=0; i<DELAYNUM; i++)
        delay_line[i] = 0.0;

    /* Main loop. */
    while (exit_signal == 0) {
        if (simple_progact == 1) { /* really-do-nothing-but-look-cool cycles. */
            wtime_musec = simple_action(&gh_data, gh_req, prg_acts,
                                        invert_flag);
            usleep(period_musec -wtime_musec);

        } else {
            switch (led_cycle) { /* These cycles actually provide info. */
                case 'L':
                    /* Toggle betw. requested prg_acts and no_blnk when idle. */
                    loc_prgacts =
                        (loc_prgacts == no_blnk) ? prg_acts : no_blnk ;

                    cpuload_action(&gh_data, gh_req,
                                   period_musec, loc_prgacts, invert_flag);
                    break;
                case 'n':
                    ifstate_action(&gh_data, gh_req, period_musec, invert_flag);
                    break;
                case 'm':
                    ifstate_action(&gh_data, gh_req, period_musec, invert_flag);
                    wtime_musec = simple_action(&gh_data, gh_req, pulse_hold,
                                                invert_flag);
                    usleep(period_musec -wtime_musec);
                    break;
                case 'N':
                    /* Toggle betw. requested prg_acts and no_blnk when idle. */
                    loc_prgacts =
                        (loc_prgacts == no_blnk) ? prg_acts : no_blnk ;

                    iftraffic_action(&gh_data, gh_req, &if_nsdata,
                                     period_musec, loc_prgacts, delay_line,
                                     forced_linkspeed, forced_iface,
                                     invert_flag);
                    break;
                default:
                    wtime_musec = simple_action(&gh_data, gh_req, 0,
                                  invert_flag);
                    usleep(period_musec -wtime_musec);
            }
        }

    } /* while */
	
    /* Leave LED in default external state & release GPIO i/o line. */
    led_onoff(&gh_data, gh_req, LEDDEFAULTEXTERNSTATE);
    ret = gpio_updown(&gh_data, &gh_req, 0);
    if (debug_level > 1) {
        fprintf(stderr, "\n%s: Info: Received signal %d (%s), exiting.\n",
                        __func__, exit_signal, strsignal(exit_signal));
    }

    return ret;
}
