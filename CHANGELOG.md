# Change Log

Changelog for TVbox-LED.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.13] - 2022-04-14

### Added
 - Show CPU load.

### Changed
 - Updated documentation.
 - Code cleanups.

## 2022-04-10

### Fixed
  - Interrupt signal handler and exit handling of gpio states.

### Changed
  - Code cleanup for if_traffic stats.

## [0.12] - 2021-02-20

### Added
 - Simple learning of max link speed based on rx/tx history (for option -b).

### Changed
 - Default build target is now a dynamically linked binary.

### Fixed
 - Scaling for number of blinks issued and blink length at given load level.

## [0.1] - 2021-01-29

### Added
 - Initial commit.
