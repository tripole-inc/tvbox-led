/*!\file
    Utility functions.
*/

#ifndef UTILITY_H
#define UTILITY_H

/*! Very simple number-to-string converter.*/
int mystr2num(char *numstr);

/*! Truncate values to [0,1]. */
float trunc_to_unitintvl(float x);

/*! Compute tabularized functions on [0,1] (only mysqrt implemented now). */
float myfcn(const char *fcn_name, float x);


#endif
